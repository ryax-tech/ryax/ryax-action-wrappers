{ stdenv
, lib
, nix2container
, runCommand
, buildEnv
, cacert
, iana-etc
, coreutils
, bashInteractive
, python
, zlib
, glib
, dotnet-runtime_6
, python3GRPCv1ActionWrapper
, python3Filev1ActionWrapper
, wrapperPackaging
, metadataDeps
}:

let
  user = "ryax";
  group = "ryax";
  uid = 1200;
  gid = 1200;
  uidStr = builtins.toString uid;
  gidStr = builtins.toString gid;
in
{ wrapperType
, actionDir
, imageName
, tag
, actionType
}:
(
  nix2container.buildImage {
    name = imageName;
    inherit tag;
    layers =
      let
        baseConfig = runCommand "base" { buildInputs = [ cacert coreutils ]; } ''
          mkdir -p $out/etc/ssl/certs/
          ln -s ${cacert}/etc/ssl/certs/ca-bundle.crt $out/etc/ssl/certs/ca-certificates.crt

          mkdir -m 1777 $out/tmp

          mkdir -p $out/usr/bin
          ln -s ${coreutils}/bin/env $out/usr/bin/env

          # Fix localhost DNS resolution
          cat > $out/etc/nsswitch.conf <<EOF
          hosts: files dns
          EOF

          # Make all library path point to /lib
          ln -s /lib $out/usr/lib
          ln -s /lib $out/usr/lib64
          ln -s /lib $out/lib64

          # create the Ryax user
          mkdir -p $out/etc/pam.d
          echo "${user}:x:${uidStr}:${gidStr}::" > $out/etc/passwd
          echo "${user}:!x:::::::" > $out/etc/shadow
          echo "${group}:x:${gidStr}:" > $out/etc/group
          echo "${group}:x::" > $out/etc/gshadow
          cat > $out/etc/pam.d/other <<EOF
          account sufficient pam_unix.so
          auth sufficient pam_rootok.so
          password requisite pam_unix.so nullok sha512
          session required pam_unix.so
          EOF
          touch $out/etc/login.defs
          mkdir -p $out/home/${user}
        '';

        baseLayer = nix2container.buildLayer {
          copyToRoot = [
            baseConfig
            (buildEnv {
              name = "base";
              paths = [
                coreutils
                python
                bashInteractive
                iana-etc
                # Default libs that are needed for some common tools
                zlib
                stdenv.cc.cc.lib
                glib
              ];
              pathsToLink = [ "/bin" "/lib" ];
            })
          ];
          perms = [
            {
              path = baseConfig;
              regex = "/home/${user}";
              mode = "0744";
              uid = uid;
              gid = gid;
              uname = user;
              gname = group;
            }
            {
              path = baseConfig;
              regex = "/tmp";
              mode = "1777";
            }
          ];
        };

        importActionData = runCommand "data" { src = actionDir; } ''
          # import function data
          mkdir -p $out/data
          files=(${actionDir}/*)
          if [ ''${#files[@]} -gt 0 ]
          then
            echo "Copying files from ${actionDir}..."
          else
            echo "ERROR: the function directory ${actionDir} is empty (or does not exist or is a file)"
            exit 1
          fi
          cp -av ${actionDir}/* $out/data
        '';
        actionDataLayer = nix2container.buildLayer {
          copyToRoot = [ importActionData ];
          perms = [
            {
              path = importActionData;
              regex = ".*";
              inherit uid gid user group;
            }
          ];
          reproducible = false;
        };

        actionNixDepsLayer = nix2container.buildLayer {
          copyToRoot = [
            (buildEnv {
              name = "actionNixDeps";
              paths = metadataDeps;
              pathsToLink = [ "/bin" "/lib" ];
            })
          ];
        };
      in
      [ baseLayer actionNixDepsLayer actionDataLayer ] ++ (if wrapperPackaging.${actionType} ? layers then (wrapperPackaging.${actionType}.layers { inherit uid gid user group; }) else [ ]);

    config =
      let
        funcTypes = rec {

          python3-grpcv1-trigger = rec {
            actionWrapper = python3GRPCv1ActionWrapper.overrideAttrs (oldAttr: {
              propagatedBuildInputs = oldAttr.propagatedBuildInputs ++ metadataDeps;
            });
            actionWrapperBin = "${actionWrapper}/bin/wrapper.py";
            extraEnv = [
              "RYAX_IS_TRIGGER_ACTION=1"
            ] ++ (
              if wrapperPackaging.${actionType} ? environment
              then wrapperPackaging.${actionType}.environment
              else [ ]
            );
          };

          python3-grpcv1 = rec {
            actionWrapper = python3GRPCv1ActionWrapper;
            actionWrapperBin = "${actionWrapper}/bin/wrapper.py";
            extraEnv = [ "RYAX_ACTION_ENTRYPOINT=${wrapperPackaging.${actionType}.entrypoint}" ] ++ (
              if wrapperPackaging.${actionType} ? environment
              then builtins.map (elem: "RYAX_USER_DEFINED_" + elem) wrapperPackaging.${actionType}.environment
              else [ ]
            );
          };

          python3-filev1 = rec {
            actionWrapper = python3Filev1ActionWrapper;
            actionWrapperBin = "${actionWrapper}/bin/wrapper.py";
            extraEnv = [ "RYAX_ACTION_ENTRYPOINT=${wrapperPackaging.${actionType}.entrypoint}" ] ++ (
              if wrapperPackaging.${actionType} ? environment
              then builtins.map (elem: "RYAX_USER_DEFINED_" + elem) wrapperPackaging.${actionType}.environment
              else [ ]
            );
          };
        };
        actionWrapperBin = funcTypes.${wrapperType}.actionWrapperBin;
        extraEnv = funcTypes.${wrapperType}.extraEnv;
      in
      {
        User = "ryax";
        WorkingDir = "/data";
        Entrypoint = [ "${actionWrapperBin}" ];
        # For Debug
        # Entrypoint = [ "${bashInteractive}/bin/bash" ];
        Env = extraEnv ++ [
          # Set home
          "HOME=/home/ryax"
          "USER=ryax"
          # Also use Ryax user in the subprocess
          "RYAX_USER_DEFINED_HOME=/home/ryax"
          "RYAX_USER_DEFINED_USER=ryax"
          # Be sure that the venv for the wrapper is accessible
          "PYTHONPATH=/data:/data/.env"
        ] ++ (
          # Fix standard lib missing error:
          # ImportError: libstdc++.so.6: cannot open shared object file: No such file or directory
          # WARNING: This is hack to avoid multiple definition of LD_LIBRARY_PATH
          (if (! builtins.any (lib.strings.hasPrefix "LD_LIBRARY_PATH=") extraEnv)
          then [ "LD_LIBRARY_PATH=/lib" ]
          else [ ]) ++ (
            if (! builtins.any (lib.strings.hasPrefix "RYAX_USER_DEFINED_LD_LIBRARY_PATH=") extraEnv)
            then [ "RYAX_USER_DEFINED_LD_LIBRARY_PATH=/lib" ]
            else [ ]
          )
        );
      };
  })
