{ lib }:
rec {
  loadLockFile = actionDir:
    let lockfile = (/. + actionDir + "/ryax_lock.json");
    in
    if !builtins.pathExists lockfile then
    # return empty attribute set
      { }
    else
      builtins.fromJSON (builtins.readFile lockfile);


  # This function `solveRequirementsTxt` determines which version of the requirements.txt file 
  # should be used to install Python dependencies. The function returns the content of the 
  # requirements.txt that can then be write into a file.
  #
  # **Return logic**:
  #    - If the content of `requirements.txt` hasn't changed (i.e., hashes match), return the locked requirements.txt.
  #    - If there are changes, return the current requirements.txt, implying dependencies should be updated.
  #
  # This ensures that dependencies are only updated if the requirements.txt file has changed.
  solveRequirementsTxt = lockfile: actionDir:
    let
      # Config md5 func
      md5 = builtins.hashString "md5";

      # Get the currently send requirements.txt
      requirements_txt = (path:
        if builtins.pathExists path then
          builtins.readFile path else "") (actionDir + "/requirements.txt");

      # create a hash of its value
      # trim function is in unstable... (24.05 is default atm)
      requirements_txt_hash = md5 requirements_txt;

      # Find the requirements_txt used for previous build (the locked one)
      locked_requirement_txt_original = (if lockfile ? requirements_txt_original then lockfile.requirements_txt_original else "");
      # Create a hash to compare
      locked_requirement_txt_original_hash = md5 locked_requirement_txt_original;

      # Load the freezed dependencies
      locked_requirement_txt = if lockfile ? requirements_txt then lockfile.requirements_txt else "";
    in
    if locked_requirement_txt != "" && locked_requirement_txt_original_hash == requirements_txt_hash then
      locked_requirement_txt
    else
      requirements_txt;

  # solvePythonVersion: Resolve the appropriate Python version from metadata and lock file.
  #
  # Parameters:
  #   - metadata: Set containing package metadata, potentially including
  #     `spec.options.python.version`.
  #   - lockFile: Set containing lock information with `python_version_original`
  #     and `python_version`.
  #
  # Returns:
  #   - A string representing the resolved Python version, or an empty string if
  #     no valid version is found. 
  #
  # Logic:
  # - Returns the locked version if it matches the original version from metadata.
  # - If the original version matches metadata, returns the metadata version.
  # - Returns the metadata version if it exists and does not match the original.
  # - Returns an empty string if no versions are present.
  solvePythonVersion = metadata: lockFile: defaultVersion:
    let
      metadataVersion =
        if metadata ? spec.options.python.version
        then metadata.spec.options.python.version
        else "";
      lockedOriginal =
        if lockFile ? python_version_original
        then lockFile.python_version_original
        else "";
      lockedVersion =
        if lockFile ? python_version
        then lockFile.python_version
        else "";
    in
    if lockedVersion != "" && lockedOriginal == metadataVersion then
      let result = lockedVersion;
      in
      builtins.trace "Using Python version from lock file: ${result}" result
    else if metadataVersion != "" then
      let result = metadataVersion;
      in
      builtins.trace "Using Python version from metadata: ${result}" result
    else
      let result = defaultVersion;
      in
      builtins.trace "Using default Python version: ${result}" result;

  # Finds a Python package in `nixpkgs` based on the specified Python version.
  # If the version exists in `nixpkgs-python.packages`, it returns that Python package.
  # Otherwise, returns an empty set (i.e., no match found).
  findPythonFromNixpkgsPython = system: nixpkgs-python: pythonVersion:
    if nixpkgs-python.packages.${system} ? ${pythonVersion} then
      nixpkgs-python.packages.${system}.${pythonVersion}
    else
      { };

  # Searches for a Python package in `pkgs` based on the major and minor Python version.
  # It ignores the third digit in the version (TODO).
  # If found, returns the package; otherwise, logs a trace and returns an empty set.
  findPythonFromNixpkgs = pkgs: pythonVersion:
    let
      # Splits the version string (e.g., "3.9.7") into a list [3, 9, 7].
      versionList = builtins.splitVersion pythonVersion;
      # Parses only the major and minor version (e.g., "3.9").
      parsedVersion = (builtins.elemAt versionList 0) + (
        if builtins.length versionList >= 2
        then builtins.elemAt versionList 1
        else ""
      );
      pythonString = "python${parsedVersion}";
    in
    if pkgs ? ${pythonString} then
      builtins.trace "Found Python version ${pythonVersion} (parsed as: ${parsedVersion}) as ${pythonString} in nixpkgs"
        pkgs.${pythonString}
    else
      builtins.trace "Python version ${pythonVersion} not found in nixpkgs" { };

  # Loads a Python package based on the given version.
  # First tries to find the package in `pkgs`, then falls back to `nixpkgs-python` if necessary.
  # Throws an error if the version is not supported.
  loadPythonPackages = system: pythonVersion: nixpkgs-python: pkgs:
    let
      pythonPackage = findPythonFromNixpkgs pkgs pythonVersion;
    in
    if pythonPackage != { } then
      pythonPackage
    else
      let
        pythonPackageFallback = findPythonFromNixpkgsPython system nixpkgs-python pythonVersion;
      in
      if pythonPackageFallback != { } then
        pythonPackageFallback
      else
        throw "Python version ${pythonVersion} is not supported.";

}
