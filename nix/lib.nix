{ pkgs, python }:
rec {
  inherit (import ./lockfile.nix { lib = pkgs.lib; })
    loadLockFile
    loadPythonPackages
    solvePythonVersion
    solveRequirementsTxt
    ;

  loadMetadataFile = actionDir:
    let metadataFile = (/. + actionDir + "/ryax_metadata.yaml");
    in
    if !builtins.pathExists metadataFile then
      abort "Could not find a ryax_metadata.yaml in the action directory."
    else
      loadYAML metadataFile;

  # This is Import From Derivation: Performance might be bad
  # https://nix.dev/manual/nix/2.22/language/import-from-derivation
  metadataDependencies2nix = actionDir: pythonPackages: actionPkgs:
    let
      metadata = loadMetadataFile actionDir;
    in
    if metadata.spec ? dependencies then
      let
        dependenciesNix = pkgs.writeScript "dependencies.nix" ''
          { pkgs, pythonPackages }:
          with pkgs;
          [ ${builtins.concatStringsSep " " metadata.spec.dependencies} ]
        '';
      in
      import dependenciesNix { inherit pythonPackages; pkgs = actionPkgs; }
    else
      [ ];

  metadataExtraEnvVars = actionDir:
    let
      metadata = loadMetadataFile actionDir;
      envList = pkgs.lib.attrsets.attrByPath [ "spec" "options" "env" ] [ ] metadata;
    in
    if (builtins.isList envList) then
      envList
    else
      throw ''
        Bad format for environment variables in the metadata file.
        Supported format is a list of bash declaration string, for example:

          env:
            - "MYVAR=foo"
            - "MY_OTHER_VAR=bar"

      '';

  loadYAML = path: builtins.fromJSON (builtins.readFile (
    pkgs.runCommand "yaml-to-json" { } ''
      ${pkgs.remarshal}/bin/remarshal -i ${path} -if yaml -of json > $out
    ''));

  toYAML = config: builtins.readFile (
    pkgs.runCommand "to-yaml" { } ''
      ${pkgs.remarshal}/bin/remarshal -i ${pkgs.writeText "to-json" (builtins.toJSON config)} -if json -of yaml > $out
    '');
  # For CI/CD
  test = pkgs.writeShellApplication {
    name = "run-tests";
    text = ''
      which python
      python --version
      which poetry
      poetry --version
      poetry config cache-dir .poetry
      poetry env use $(which python)
      poetry config --list

      poetry install

      export LD_LIBRARY_PATH=${pkgs.lib.makeLibraryPath [pkgs.stdenv.cc.cc.lib]}
      poetry run bash test.sh $@

    '';
    runtimeInputs = [ python pkgs.poetry pkgs.bashInteractive ];
    checkPhase = "";
  };

  lint = pkgs.writeShellApplication {
    name = "run-lint";
    text = ''
      python --version
      poetry --version
      poetry install
      LINT_CMD=''${LINT_CMD:-./lint.sh}
      poetry run bash "$LINT_CMD" $@
    '';
    runtimeInputs = [ python pkgs.poetry pkgs.bashInteractive ];
    checkPhase = "";
  };
}
