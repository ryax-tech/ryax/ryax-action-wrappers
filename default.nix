{ actionNixpkgs
, actionDir
, nix2container
, nixpkgs-python
, system
, ryax-lib
}:
let
  # Apply overlays from overlays.nix if exists
  overlays_file = "${actionDir}/overlays.nix";
  overlays = if builtins.pathExists overlays_file then builtins.trace "Using local overlays in ${overlays_file}" import overlays_file else [ ];
  pkgs = import actionNixpkgs { inherit overlays; };
  lockTmpDir = ".lock_tmp";

  # load libs and tools
  inherit (pkgs) callPackage;

  metadataDeps = ryax-lib.metadataDependencies2nix actionDir python.pkgs pkgs;
  extraEnvVars = ryax-lib.metadataExtraEnvVars actionDir;

  # Default python version
  defaultPythonVersion = "3";

  metadata = ryax-lib.loadMetadataFile (/. + actionDir);
  inLockFile = ryax-lib.loadLockFile (/. + actionDir);

  # Find the provided python and nixpkgs version by the user
  # This is use for the locking and environment control
  lockOriginalPythonVersion = pkgs.lib.attrsets.attrByPath
    [ "spec" "options" "python" "version" ] ""
    metadata;
  lockOriginalNixpkgsVersion = pkgs.lib.attrsets.attrByPath
    [ "spec" "options" "nixpkgs" "version" ] ""
    metadata;

  python = ryax-lib.loadPythonPackages system (ryax-lib.solvePythonVersion metadata inLockFile defaultPythonVersion)
    nixpkgs-python
    pkgs;
  pythonSolver = ryax-lib.loadPythonPackages system (ryax-lib.solvePythonVersion metadata inLockFile defaultPythonVersion)
    nixpkgs-python;

  requirementsTxt = builtins.toFile "requirements.txt" (ryax-lib.solveRequirementsTxt inLockFile actionDir);

  pythonPackages = python.pkgs;

  # Give Action Wrappers to the builder
  python3GRPCv1ActionWrapper = pkgs.callPackage ./wrappers/python3-grpcv1/wrapper.nix { inherit pythonPackages; };
  python3Filev1ActionWrapper = pkgs.callPackage ./wrappers/python3-filev1/wrapper.nix { inherit pythonPackages; };

  actionBuilder = callPackage ./nix/ryax-action-build.nix {
    inherit metadataDeps
      python3GRPCv1ActionWrapper
      python3Filev1ActionWrapper
      python
      nix2container
      wrapperPackaging;
  };
  wrapperPackaging = import ./languages {
    inherit
      actionDir
      actionNixpkgs
      extraEnvVars
      metadataDeps
      nix2container
      overlays
      pkgs
      requirementsTxt
      python
      lockTmpDir
      pythonSolver;
  };

  # Load build metadata form the build file
  ryaxBuild = ryax-lib.loadYAML (/. + actionDir + "/ryax_build.yaml");
  imageName =
    if (ryaxBuild.spec ? registry && ryaxBuild.spec.registry != null)
    then ryaxBuild.spec.registry + "/" + ryaxBuild.spec.name
    else ryaxBuild.spec.name;
  actionType = ryaxBuild.spec.type;
  imageVersion = ryaxBuild.spec.version;
  wrapperType = ryaxBuild.spec.wrapperType;

  outLockFile = {
    actionNix = actionNixpkgs.rev;
    actionNixOriginal = lockOriginalNixpkgsVersion;
    nixpkgsPython = nixpkgs-python.rev;
    python = {
      pythonVersionOriginal = lockOriginalPythonVersion;
      pythonVersion = python.version;
      lockedRequirements = (path:
        if builtins.pathExists path
        then builtins.readFile path
        else "") ("${actionDir}/${lockTmpDir}/requirements.lock.txt");
      originalRequirements = (path:
        if builtins.pathExists path
        then builtins.readFile path
        else "") ("${actionDir}/${lockTmpDir}/requirements.orig.txt");
    };
    wrapperRevision = ryaxBuild.spec.wrapperRev;
  };
in
{

  build = actionBuilder {
    inherit imageName actionType wrapperType;
    actionDir = (/. + actionDir);
    tag = imageVersion;
  };

  # Run this to prepare package before build
  package = wrapperPackaging;

  lock = pkgs.writeShellScriptBin "lock" ''
    cat <<EOF
    ${builtins.toJSON outLockFile}
    EOF
  '';
}
