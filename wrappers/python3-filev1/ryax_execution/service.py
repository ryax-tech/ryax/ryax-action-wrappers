# Copyright 2022 Ryax Technologies
# Use of this trigger code is governed by a BSD-style
# license that can be found in the LICENSE file.
import asyncio
import json
import logging
import os
import platform
import shutil
from logging import getLogger
from pathlib import Path
from hashlib import md5

import yaml

logger = getLogger(__name__)
__version__ = "0.1.0"

python_version = platform.python_version()

to_clean_list: list = []

USER_DEFINED_ENV_PREFIX = "RYAX_USER_DEFINED_"


class WrapperExecutionError(Exception):
    pass


def clean_files() -> None:
    for to_clean in to_clean_list:
        if Path(to_clean).is_dir():
            shutil.rmtree(to_clean)
        elif Path(to_clean).is_file():
            os.remove(to_clean)


async def _run_user_code(input_dict: dict, input_dir: Path) -> dict:
    wrapper_command = os.environ["RYAX_ACTION_ENTRYPOINT"].split()
    logger.debug("Start wrapper subprocess")
    working_dir = Path("/tmp")
    with open(working_dir / "inputs.json", "w") as f_ref:
        f_ref.write(json.dumps(input_dict, indent=4))
    process = await asyncio.create_subprocess_exec(
        *wrapper_command,
        working_dir,
        stdout=asyncio.subprocess.PIPE,
        stderr=asyncio.subprocess.STDOUT,
        env={
            var_name.removeprefix(USER_DEFINED_ENV_PREFIX): var_value
            for var_name, var_value in os.environ.items()
            if var_name.startswith(USER_DEFINED_ENV_PREFIX)
        },
    )
    # Real time output of the action logs
    while True:
        if process.stdout.at_eof():
            break
        stdout = (await process.stdout.readline()).decode()
        if stdout:
            print(stdout, end="", flush=True)

    await process.wait()
    if process.returncode != 0:
        raise WrapperExecutionError(
            f"The wrapper process execution failed with exit code: {process.returncode}"
        )
    logger.debug("Wrapper subprocess ended")
    await process.wait()
    with open(working_dir / "outputs.json", "r") as outputs_json:
        output_dict = json.load(outputs_json)
    return output_dict


async def _md5(file_path: Path) -> str:
    m = md5()
    chunk_size = 8192
    with open(file_path, "rb") as f:
        while True:
            chunk = f.read(chunk_size)
            if len(chunk):
                m.update(chunk)
            else:
                break
    return str(m.hexdigest())


async def execute_handler(iodir: Path, ryax_metadata: str) -> None:
    """
    Execute the handle function on user defined handler, depends on the
    used programming language.
    This simple wrapper uses
    files to read all input from the filesystem, it also writes all output to the filesystem,
    needs to have read and write access to a folder with subdirs inputs and outputs.
    See more on README.md.

    Subdir inputs has at least a file inputs.json that carries all the input values.
    Subdir outputs will produce outputs.json that contains all the output values when execution finishes.

    Directories and files are also available inside the inputs/outputs subdirs.
    """
    yaml_file = yaml.load(open(ryax_metadata), Loader=yaml.Loader)
    if yaml_file["apiVersion"] != "ryax.tech/v2.0":
        logger.error("Unsupported ryax_metadata.yaml file version.")
        exit(1)
    output_definitions = yaml_file["spec"].get("outputs", [])
    input_definitions = yaml_file["spec"].get("inputs", [])

    with open(Path(iodir) / "inputs.json", "rb") as json_ref:
        input_dict = json.load(json_ref)

    # Prepare directory input
    for input_def in input_definitions:
        if input_def["type"] == "directory":
            zip_md5 = await _md5(Path(input_dict[input_def["name"]]))
            dir_parameter_path = Path(iodir) / f"inputs/{zip_md5}"
            if not dir_parameter_path.is_dir():
                dir_parameter_path.mkdir()
                shutil.unpack_archive(
                    input_dict[input_def["name"]], extract_dir=dir_parameter_path
                )
            input_dict[input_def["name"]] = str(dir_parameter_path)
    # Read data from NFS, values for directories and files should be full paths in /iodir/inputs/
    output_dir = Path(iodir) / "outputs"

    # Execute using the data
    output_dict = await _run_user_code(input_dict, iodir)

    # Finally copy the output to the shared output directory, make sure directories become archives
    for outputdef in output_definitions:
        output_name = outputdef["name"]
        # If the output is not present accept only in case it is optional
        if output_name in output_dict:
            output_value = output_dict[output_name]
        elif "optional" in outputdef and outputdef["optional"]:
            continue
        else:
            raise ValueError(
                f"Missing mandatory output: {output_name}, to set an output as optional: add attribute 'optional: true' on ryax_metdatada.yaml!"
            )
        # If it is a directory create a compressed .zip archive
        if outputdef["type"] == "directory":
            output_archive = Path(output_dir) / Path(output_value).name
            shutil.make_archive(output_archive, "zip", root_dir=output_value)
            # Always get path relative to output dir
            output_dict[output_name] = str(output_archive) + ".zip"
            # Delete the directory and keep only the archive
            to_clean_list.append(output_value)
        # If it is a file just copy the file to the shared output directory
        elif outputdef["type"] == "file":
            shutil.copy(output_value, output_dir)
            # Get path relative to outputs dir
            output_dict[output_name] = str(output_dir / Path(output_value).name)
    with open(Path(iodir) / "outputs.json", "w") as output_file:
        json.dump(output_dict, output_file)


def main(
    str_log_level: str,
    clean_files_between_execs: bool,
):
    numeric_level = getattr(logging, str_log_level.upper(), None)
    if not isinstance(numeric_level, int):
        raise ValueError("Invalid log level: %s" % str_log_level)
    logging.basicConfig(
        level=numeric_level,
        format="%(asctime)s.%(msecs)03d %(levelname)-7s %(name)-22s %(message)s",
        datefmt="%Y-%m-%d,%H:%M:%S",
    )
    logger.debug("Logging level is set to %s" % str_log_level.upper())
    logger.debug(f"Ryax wrapper GRPC version {__version__}")
    logger.debug(f"Python version {python_version}")

    logger.debug(
        f"Remove temporary data between executions: {clean_files_between_execs}"
    )
    asyncio.run(execute_handler(Path("/iodir"), "/data/ryax_metadata.yaml"))
    if clean_files_between_execs:
        clean_files()
