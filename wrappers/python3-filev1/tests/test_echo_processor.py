# Copyright 2022 Ryax Technologies
# Use of this trigger code is governed by a BSD-style
# license that can be found in the LICENSE file.
import asyncio
import json
import os
import shutil
import tempfile
from filecmp import cmp
from pathlib import Path
from tempfile import mkdtemp

from ryax_execution.service import execute_handler


def test_input_dir_file_str() -> None:  # type: ignore
    test_dir = mkdtemp()

    input_dir = Path(test_dir) / "inputs"
    if input_dir.is_dir():
        shutil.rmtree(input_dir)
    Path.mkdir(input_dir)

    output_dir = Path(test_dir) / "outputs"
    if output_dir.is_dir():
        shutil.rmtree(output_dir)
    Path.mkdir(output_dir)

    input_dict = {
        "test_str": "name",
        "test_dir": str(input_dir / "test_dir.zip"),
        "test_file": "tests-file.txt",
        "test_int": 1000,
        "test_float": 3.14e-19,
    }

    os.environ["RYAX_ACTION_ENTRYPOINT"] = "python3 /tmp/echo_processor/ryax_wrapper.py"
    os.environ["RYAX_USER_DEFINED_PATH"] = os.environ["PATH"]
    os.environ["RYAX_USER_DEFINED_PYTHONPATH"] = "examples/echo_processor/"

    # Create tests input file
    input_dict["test_file"] = str(input_dir / input_dict["test_file"])
    with open(input_dict["test_file"], "w") as text_file:
        text_file.write("Hello")

    # Create tests input directory with 2 text files, directories come as zip files
    with tempfile.TemporaryDirectory() as tmpdirname:
        for i in ["test_file1.txt", "test_file2.txt"]:
            with open(Path(tmpdirname) / i, "w") as text_file:
                text_file.write(f"Hello from {i}")
        shutil.make_archive(input_dir / "test_dir", "zip", root_dir=tmpdirname)

    with open(Path(test_dir) / "inputs.json", "w") as json_file:
        json_file.write(json.dumps(input_dict, indent=4))

    asyncio.run(execute_handler(test_dir, "examples/echo_processor/ryax_metadata.yaml"))

    assert Path("/tmp/outputs.json").exists()

    # Read all output comparing it to the expected input
    with open(Path(test_dir) / "outputs.json", "r") as output_json:
        output_dict = json.loads(output_json.read())
        for key in output_dict:
            if "file" in key:
                output_dict_final = Path(output_dir) / output_dict[key]
                assert cmp(input_dict[key], output_dict_final)
            elif "int" in key:
                assert output_dict[key] == int(input_dict[key])
            elif "float" in key:
                assert output_dict[key] == float(input_dict[key])
            elif "dir" not in key:
                assert output_dict[key] == input_dict[key]

    assert output_dir.exists()
    assert input_dir.exists()
