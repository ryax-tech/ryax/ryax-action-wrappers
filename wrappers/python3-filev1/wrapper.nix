{ stdenv, pythonPackages }:
pythonPackages.buildPythonPackage {
  pname = "ryax-action-wrapper-python3-filev1";
  version = "1";

  src = ./.;

  buildInputs = [ pythonPackages.setuptools_scm ];

  propagatedBuildInputs = [
    pythonPackages.pyyaml
  ];

  doCheck = false;

  meta = with stdenv.lib; {
    homepage = "https://ryax.tech/";
    description = "Ryax action wrapper for Python3 using simple files v1";
  };
}
