# Copyright 2022 Ryax Technologies
# Use of this trigger code is governed by a BSD-style
# license that can be found in the LICENSE file.
from setuptools import find_packages, setup

setup(
    name="ryax-action-wrapper-python3-file",
    version="1",
    scripts=["./wrapper.py"],
    description="Ryax wrapper for python3 actions",
    packages=find_packages(),
)
