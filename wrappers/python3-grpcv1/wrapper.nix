{ stdenv, pythonPackages }:
pythonPackages.buildPythonPackage {
  pname = "ryax-action-wrapper-python3-grpc";
  version = "1";

  src = ./.;

  buildInputs = [ pythonPackages.setuptools_scm ];

  propagatedBuildInputs = [
    pythonPackages.grpcio
    pythonPackages.protobuf
  ];

  doCheck = false;

  meta = with stdenv.lib; {
    homepage = "https://ryax.tech/";
    description = "Ryax action wrapper for Python3 gRPC v1";
  };
}
