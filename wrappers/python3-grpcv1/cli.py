#!/usr/bin/env python3
import asyncio
import os
import re
import shutil
import sys
import tempfile
from pathlib import Path
from typing import Dict, Iterator, List

import grpc
import yaml
from google import protobuf
from google.protobuf.any_pb2 import Any
from google.protobuf.empty_pb2 import Empty
from ryax_execution import execution_v3_pb2
from ryax_execution.execution_v3_pb2 import (
    InitReply,
    InitRequest,
    IODirectoryValue,
    IOFileValue,
    IOFloatValue,
    IOIntegerValue,
    IOType,
    IOValue,
    RunReply,
    RunRequest,
)
from ryax_execution.execution_v3_pb2_grpc import ActionExecutionStub


class TCOLOR:
    HEADER = "\033[95m"
    OKBLUE = "\033[94m"
    OKGREEN = "\033[92m"
    WARNING = "\033[93m"
    FAIL = "\033[91m"
    BOLD = "\033[1m"
    UNDERLINE = "\033[4m"
    ENDC = "\033[0m"


LEN_DATA_SENT = 4_000_000 - 10_000  # keep some space for metainfos


def convert_protobuf_time_to_timestamp(t: protobuf.timestamp_pb2.Timestamp) -> float:
    """
    Return a floating timestamp in UTC+00.
    Time is stored in UTC in protobuf, thus, there is not so much things to do.
    """
    return t.ToNanoseconds() / 1000_000_000


async def run(
    server_ip: str,
    output_dir: str,
    input_defs: List[Dict[str, Any]],
    output_defs: List[Dict[str, Any]],
) -> None:
    print(f"Starting client, connecting to {server_ip}...")
    async with grpc.aio.insecure_channel(server_ip) as channel:
        stub = ActionExecutionStub(channel)

        version = await stub.Version(Empty())
        print(f"Module Wrapper version: {version.version}")

        output_types = []
        for outp in output_defs:
            output_types.append(
                IOType(name=outp["name"], type=IOType.Types.Value(outp["type"].upper()))
            )

        response = await stub.Init(InitRequest(output_types=output_types))
        print(f"Init response received:\n{response}")
        if response.status == InitReply.ERROR:
            raise Exception(f"Error while initializing the execution: {response}")

        inputs = []
        for idef in input_defs:
            if idef["type"] in ["string", "password", "longstring", "enum"]:
                inputs.append(IOValue(name=idef["name"], value=idef["value"]))
            elif idef["type"] == "float":
                inputs.append(
                    IOFloatValue(name=idef["name"], value=float(idef["value"]))
                )
            elif idef["type"] == "integer":
                inputs.append(
                    IOIntegerValue(name=idef["name"], value=int(idef["value"]))
                )
            elif idef["type"] == "file":
                filepath = idef["value"]
                with open(filepath, "rb") as output_file:
                    data = output_file.read(LEN_DATA_SENT)
                    while len(data) > 0:
                        inputs.append(
                            IOFileValue(
                                name=idef["name"],
                                value=data,
                                file_name=os.path.basename(filepath),
                            )
                        )
                        data = output_file.read(LEN_DATA_SENT)
            elif idef["type"] == "directory":
                output_name = idef["name"]
                out_dir = Path(idef["value"])
                if not out_dir.exists():
                    raise Exception(
                        f"The output reference '{out_dir}' for the output named '{output_name}' does not exists"
                    )
                if not out_dir.is_dir():
                    raise Exception(
                        f"The output reference '{out_dir}' for the output named '{output_name}' is not a directory"
                    )
                with tempfile.NamedTemporaryFile(suffix=".zip") as fp:
                    shutil.make_archive(
                        fp.name[: -len(".zip")], "zip", root_dir=out_dir
                    )
                    with open(fp.name, "rb") as output_file:
                        data = output_file.read(LEN_DATA_SENT)
                        while True:
                            inputs.append(
                                IODirectoryValue(
                                    name=output_name,
                                    value=data,
                                    is_last_chunk=len(data) < LEN_DATA_SENT,
                                )
                            )
                            data = output_file.read(LEN_DATA_SENT)
                            if len(data) == 0:
                                break

            else:
                print(f"Unsupported input type '{idef['type']}'.")
                exit(1)

        def generate_inputs() -> Iterator[RunRequest]:
            for inp in inputs:
                req = RunRequest()
                req.input.Pack(inp)
                yield req

        print("Sending a run request...")
        already_recieved_files = set()
        os.makedirs(f"{output_dir}/RYAX_TEMPORARY", exist_ok=True)
        outputs = {}
        async for response in stub.Run(generate_inputs()):
            if response.last_reply_of_this_run:
                status = RunReply.Status.Name(response.status)
                if status == "DONE":
                    status = f"{TCOLOR.OKGREEN}DONE{TCOLOR.ENDC}"
                elif status == "ERROR":
                    status = f"{TCOLOR.FAIL}ERROR{TCOLOR.ENDC}"
                else:
                    status = f"{TCOLOR.OKBLUE}{status}{TCOLOR.ENDC}"
                print(f"{TCOLOR.BOLD}New Response{TCOLOR.ENDC} with status: {status}")
                print(
                    f"Execution started at {convert_protobuf_time_to_timestamp(response.start_time)} and ended at {convert_protobuf_time_to_timestamp(response.end_time)}"
                )
                for outp, outv in outputs.items():
                    print(f"- {outp}: {outv}")
                outputs = {}
            else:
                output_file_type_str = response.output.TypeName().split(".")[-1]
                output_type = getattr(execution_v3_pb2, output_file_type_str)
                output = output_type()
                response.output.Unpack(output)
                if output_file_type_str == "IOFileValue":
                    filepath: str = f"{output_dir}/{output.name}/{output.file_name}"
                    if filepath not in already_recieved_files:
                        already_recieved_files.add(filepath)
                        if os.path.exists(filepath):
                            os.remove(filepath)
                    os.makedirs(os.path.dirname(filepath), exist_ok=True)
                    with open(filepath, mode="ab") as input_file:
                        input_file.write(output.value)
                    outputs[output.name] = filepath
                elif output_file_type_str == "IODirectoryValue":
                    filepath = f"{output_dir}/RYAX_TEMPORARY/{output.name}.zip"
                    if filepath not in already_recieved_files:
                        already_recieved_files.add(filepath)
                        if os.path.exists(filepath):
                            os.remove(filepath)
                    with open(filepath, mode="ab") as input_file:
                        input_file.write(output.value)
                    if output.is_last_chunk:
                        os.makedirs(f"{output_dir}/{output.name}", exist_ok=True)
                        shutil.unpack_archive(
                            filepath, extract_dir=f"{output_dir}/{output.name}"
                        )
                        outputs[output.name] = f"{output_dir}/{output.name}"
                else:
                    outputs[output.name] = output.value
        shutil.rmtree(f"{output_dir}/RYAX_TEMPORARY")
        if "ERROR" in status:
            exit(1)


def parse_cli_args(argv: List[str]):
    input_values = {}
    server = "localhost:8081"
    file = None
    output_dir = "/tmp"

    while len(argv) > 0:
        arg = argv.pop(0)
        if arg == "--help":
            raise ValueError("")
        elif arg.startswith("--server="):
            server = arg[len("--server=") :]
        elif arg.startswith("--output-dir="):
            output_dir = arg[len("--output-dir=") :]
        else:
            file = arg
            break

    if file is None:
        raise ValueError("Missing `ryax_metadata.yaml` file.")

    for arg in argv:
        if arg.startswith("--"):
            m = re.fullmatch(r"--(\w+(?:-\w+)*)=(.*)", arg)
            if m is None:
                raise ValueError(f"Argument '{arg}' do not follow the format.")
            input_values[m.group(1)] = m.group(2)
        else:
            raise ValueError(
                f"Argument '{arg}' do not follow the format. It shoud start with '--'"
            )
    return server, file, input_values, output_dir


if __name__ == "__main__":
    try:
        server, file, input_values, output_dir = parse_cli_args(sys.argv[1:])
    except ValueError as e:
        msg = e.args[0]
        if msg != "":
            print(e.args[0])
            print("")
        print(
            "usage: cli.py [--help] [--server=<url>] <path/to/ryax_metadata.yaml> [--<input_name>=<value>] [--<input_name>=<value>] ..."
        )
        print("")
        print("\t--server=<url>\t Module <url>")
        print(
            "\t--output-dir=<url>\t If the action outputs some files or directories, write them there. Default: /tmp."
        )
        print(
            "\t<path/to/ryax_metadata.yaml>\t is a path to a valid `ryax_metadata.yaml` file."
        )
        print(
            "\t--<input_name>=<value>\t Set the value <value> for the input <input_name>. Should be defined for each input."
        )
        exit(1)

    yaml_file = yaml.load(open(file), Loader=yaml.Loader)
    if yaml_file["apiVersion"] != "ryax.tech/v2.0":
        print("Unsupported ryax_metadata.yaml file version.")
        exit(1)

    input_definitions = yaml_file["spec"].get("inputs", [])
    output_definitions = yaml_file["spec"].get("outputs", [])

    input_defs = []

    for inputdef in input_definitions:
        if inputdef["name"] not in input_values.keys():
            if inputdef.get("optional"):
                print(f"WARNING {inputdef['name']} is optional and was not specified!")
                print(
                    f"        To define {inputdef['name']} in the CLI: add --{inputdef['name']}=somevalue"
                )
            else:
                print(
                    f"Please, define {inputdef['name']} in the CLI: add --{inputdef['name']}=somevalue"
                )
                exit(1)
        else:
            inputdef["value"] = input_values[inputdef["name"]]
            input_values.pop(inputdef["name"])
            input_defs.append(inputdef)

    if len(input_values) > 0:
        print(
            f"WARNING: You have defined the following input values but they are useless: {input_values.keys()}."
        )

    asyncio.run(run(server, output_dir, input_defs, output_definitions))
