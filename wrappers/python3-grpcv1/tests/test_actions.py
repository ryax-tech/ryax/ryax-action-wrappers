# Copyright 2022 Ryax Technologies
# Use of this trigger code is governed by a BSD-style
# license that can be found in the LICENSE file.
from typing import AsyncIterator, Callable

import pytest
from google.protobuf.any_pb2 import Any as AnyProtobuf
from ryax_execution import execution_v3_pb2
from ryax_execution.execution_v3_pb2 import (
    InitReply,
    InitRequest,
    IOType,
    IOValue,
    RunReply,
    RunRequest,
)
from ryax_execution.service import ActionExecutionService
from test_common import ServicerContextMockup


@pytest.mark.asyncio
async def test_run_no_inputs_no_outputs() -> None:
    service = ActionExecutionService(clean_files_between_execs=False)
    req = InitRequest(output_types=[])
    ctx = ServicerContextMockup()
    init_rep: InitReply = service.Init(req, ctx)

    async def test_import_user_code(input_dict):
        import ryax_handler

        return ryax_handler.handle(input_dict)

    service._run_user_code = test_import_user_code
    assert init_rep.status == InitReply.Status.DONE
    assert init_rep.executor_id != ""
    assert init_rep.log_delimiter != ""

    async def generator() -> AsyncIterator[RunRequest]:
        yield RunRequest()

    reps = []
    async for a in service.Run(generator(), ctx):
        reps.append(a)

    assert len(reps) == 1
    assert reps[0].last_reply_of_this_run is True
    assert reps[0].next_log_delimiter != init_rep.log_delimiter
    assert reps[0].status == RunReply.Status.DONE


@pytest.mark.asyncio
async def test_run_unknown_output() -> None:
    service = ActionExecutionService(clean_files_between_execs=False)
    req = InitRequest(output_types=[])
    ctx = ServicerContextMockup()
    init_rep: InitReply = service.Init(req, ctx)
    assert init_rep.status == InitReply.Status.DONE
    assert init_rep.executor_id != ""
    assert init_rep.log_delimiter != ""

    async def generator() -> AsyncIterator[RunRequest]:
        inp = AnyProtobuf()
        inp.Pack(IOValue(name="mystring", value="test_value"))
        yield RunRequest(input=inp)

    reps = []
    async for a in service.Run(generator(), ctx):
        reps.append(a)

    assert len(reps) == 1
    assert reps[0].last_reply_of_this_run is True
    assert reps[0].next_log_delimiter != init_rep.log_delimiter
    assert reps[0].status == RunReply.Status.ERROR


@pytest.mark.asyncio
async def test_run() -> None:
    service = ActionExecutionService(clean_files_between_execs=False)
    req = InitRequest(output_types=[IOType(name="mystring", type=IOType.STRING)])
    ctx = ServicerContextMockup()
    init_rep: InitReply = service.Init(req, ctx)
    assert init_rep.status == InitReply.Status.DONE
    assert init_rep.executor_id != ""
    assert init_rep.log_delimiter != ""

    async def test_import_user_code(input_dict):
        import ryax_handler

        return ryax_handler.handle(input_dict)

    service._run_user_code = test_import_user_code

    async def generator() -> AsyncIterator[RunRequest]:
        inp = AnyProtobuf()
        inp.Pack(IOValue(name="mystring", value="test_value"))
        yield RunRequest(input=inp)

    reps = []
    async for a in service.Run(generator(), ctx):
        reps.append(a)

    assert len(reps) == 2

    output_file_type_str = reps[0].output.TypeName().split(".")[-1]
    output_type = getattr(execution_v3_pb2, output_file_type_str)
    output = output_type()
    reps[0].output.Unpack(output)

    assert output.value == "test_value"
    assert output.name == "mystring"

    assert reps[1].last_reply_of_this_run is True
    assert reps[1].next_log_delimiter != init_rep.log_delimiter
    assert reps[1].status == RunReply.Status.DONE


@pytest.mark.asyncio
async def test_run_cant_import_code() -> None:
    service = ActionExecutionService(clean_files_between_execs=False)

    def _import_user_code_raise() -> Callable[[dict], dict]:
        raise ImportError

    ActionExecutionService._import_user_code = _import_user_code_raise

    req = InitRequest(output_types=[IOType(name="mystring", type=IOType.STRING)])
    ctx = ServicerContextMockup()
    init_rep: InitReply = service.Init(req, ctx)
    assert init_rep.status == InitReply.Status.DONE
    assert init_rep.executor_id != ""
    assert init_rep.log_delimiter != ""

    async def generator() -> AsyncIterator[RunRequest]:
        inp = AnyProtobuf()
        inp.Pack(IOValue(name="mystring", value="test_value"))
        yield RunRequest(input=inp)

    reps = []
    async for a in service.Run(generator(), ctx):
        reps.append(a)

    assert len(reps) == 1
    assert reps[0].last_reply_of_this_run is True
    assert reps[0].next_log_delimiter != init_rep.log_delimiter
    assert reps[0].status == RunReply.Status.ERROR


@pytest.mark.asyncio
async def test_run_user_code_fails() -> None:
    service = ActionExecutionService(clean_files_between_execs=False)

    def _import_user_code() -> Callable[[dict], dict]:
        def handle(d: dict) -> dict:
            raise Exception("arg")

        return handle

    ActionExecutionService._import_user_code = _import_user_code

    req = InitRequest(output_types=[IOType(name="mystring", type=IOType.STRING)])
    ctx = ServicerContextMockup()
    init_rep: InitReply = service.Init(req, ctx)
    assert init_rep.status == InitReply.Status.DONE
    assert init_rep.executor_id != ""
    assert init_rep.log_delimiter != ""

    async def generator() -> AsyncIterator[RunRequest]:
        inp = AnyProtobuf()
        inp.Pack(IOValue(name="mystring", value="test_value"))
        yield RunRequest(input=inp)

    reps = []
    async for a in service.Run(generator(), ctx):
        reps.append(a)

    assert len(reps) == 1
    assert reps[0].last_reply_of_this_run is True
    assert reps[0].next_log_delimiter != init_rep.log_delimiter
    assert reps[0].status == RunReply.Status.ERROR


@pytest.mark.asyncio
async def test_run_missing_output() -> None:
    service = ActionExecutionService(clean_files_between_execs=False)

    def _import_user_code() -> Callable[[dict], dict]:
        def handle(d: dict) -> dict:
            return {}

        return handle

    ActionExecutionService._import_user_code = _import_user_code

    req = InitRequest(output_types=[IOType(name="mystring", type=IOType.STRING)])
    ctx = ServicerContextMockup()
    init_rep: InitReply = service.Init(req, ctx)
    assert init_rep.status == InitReply.Status.DONE
    assert init_rep.executor_id != ""
    assert init_rep.log_delimiter != ""

    async def generator() -> AsyncIterator[RunRequest]:
        inp = AnyProtobuf()
        inp.Pack(IOValue(name="mystring", value="test_value"))
        yield RunRequest(input=inp)

    reps = []
    async for a in service.Run(generator(), ctx):
        reps.append(a)
    print(reps)
    assert len(reps) == 1
    assert reps[0].last_reply_of_this_run is True
    assert reps[0].next_log_delimiter != init_rep.log_delimiter
    assert reps[0].status == RunReply.Status.ERROR
