# Copyright 2022 Ryax Technologies
# Use of this trigger code is governed by a BSD-style
# license that can be found in the LICENSE file.
import glob
import os
import random
import shutil
import tempfile
from typing import Any

import pytest
from google.protobuf.any_pb2 import Any as AnyProtobuf
from ryax_execution import execution_v3_pb2
from ryax_execution.execution_v3_pb2 import (
    IOBooleanValue,
    IOBytesValue,
    IODirectoryValue,
    IOFileValue,
    IOFloatValue,
    IOIntegerValue,
    IOType,
    IOValue,
)
from ryax_execution.service import LEN_DATA_SENT, ActionExecutionService, ActionIOType

SMALL_ZIP_FILE = (
    b"PK\x03\x04\x14\x00\x00\x00\x00\x00\xc1`\x88S\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x05"
    b"\x00\x00\x00adir/PK\x03\x04\x14\x00\x00\x00\x00\x00\xa2`\x88S\x8b\x9e\xd9\xd3\x01\x00\x00\x00\x01"
    b"\x00\x00\x00\n\x00\x00\x00adir/afileAPK\x03\x04\x14\x00\x00\x00\x00\x00\xc1`\x88S\x00\x00\x00\x00"
    b"\x00\x00\x00\x00\x00\x00\x00\x00\r\x00\x00\x00adir/asubdir/PK\x03\x04\x14\x00\x00\x00\x00\x00\x9c"
    b"`\x88S1\xcf\xd0J\x01\x00\x00\x00\x01\x00\x00\x00\x18\x00\x00\x00adir/asubdir/anotherfileBPK\x03"
    b"\x04\x14\x00\x00\x00\x00\x00\xaa`\x88S\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x0e\x00\x00"
    b"\x00adir/emptyfilePK\x03\x04\x14\x00\x00\x00\x00\x00\xc1`\x88S\x00\x00\x00\x00\x00\x00\x00\x00\x00"
    b"\x00\x00\x00\x0e\x00\x00\x00adir/emptydir/PK\x01\x02?\x03\x14\x00\x00\x00\x00\x00\xc1`\x88S\x00"
    b"\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x05\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\xedA"
    b"\x00\x00\x00\x00adir/PK\x01\x02?\x03\x14\x00\x00\x00\x00\x00\xa2`\x88S\x8b\x9e\xd9\xd3\x01\x00\x00"
    b"\x00\x01\x00\x00\x00\n\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\xa4\x81#\x00\x00\x00adir"
    b"/afilePK\x01\x02?\x03\x14\x00\x00\x00\x00\x00\xc1`\x88S\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00"
    b"\x00\x00\r\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\xedAL\x00\x00\x00adir/asubdir/PK\x01\x02"
    b"?\x03\x14\x00\x00\x00\x00\x00\x9c`\x88S1\xcf\xd0J\x01\x00\x00\x00\x01\x00\x00\x00\x18\x00\x00\x00"
    b"\x00\x00\x00\x00\x00\x00\x00\x00\xa4\x81w\x00\x00\x00adir/asubdir/anotherfilePK\x01\x02?\x03\x14"
    b"\x00\x00\x00\x00\x00\xaa`\x88S\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x0e\x00\x00\x00\x00"
    b"\x00\x00\x00\x00\x00\x00\x00\xa4\x81\xae\x00\x00\x00adir/emptyfilePK\x01\x02?\x03\x14\x00\x00\x00"
    b"\x00\x00\xc1`\x88S\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x0e\x00\x00\x00\x00\x00\x00\x00"
    b"\x00\x00\x00\x00\xedA\xda\x00\x00\x00adir/emptydir/PK\x05\x06\x00\x00\x00\x00\x06\x00\x06\x00d\x01"
    b"\x00\x00\x06\x01\x00\x00\x00\x00 "
)


@pytest.mark.asyncio
async def test_prepare_input() -> None:
    tmp_dir = tempfile.TemporaryDirectory()

    inp = IOValue(name="mystring", value="test_value")
    req = AnyProtobuf()
    req.Pack(inp)
    prep_input = ActionExecutionService._prepare_input(tmp_dir, req)
    assert prep_input[0] == "mystring"
    assert prep_input[1] == "test_value"

    inp = IOBytesValue(name="mybytes", value=b"test_value")
    req = AnyProtobuf()
    req.Pack(inp)
    prep_input = ActionExecutionService._prepare_input(tmp_dir, req)
    assert prep_input[0] == "mybytes"
    assert prep_input[1] == b"test_value"

    inp = IOIntegerValue(name="myinteger", value=123)
    req = AnyProtobuf()
    req.Pack(inp)
    prep_input = ActionExecutionService._prepare_input(tmp_dir, req)
    assert prep_input[0] == "myinteger"
    assert prep_input[1] == 123

    inp = IOFloatValue(name="myfloat", value=0.123)
    req = AnyProtobuf()
    req.Pack(inp)
    prep_input = ActionExecutionService._prepare_input(tmp_dir, req)
    assert prep_input[0] == "myfloat"
    assert abs(prep_input[1] - 0.123) < 0.0000001

    inp = IOBooleanValue(name="myboolean", value=True)
    req = AnyProtobuf()
    req.Pack(inp)
    prep_input = ActionExecutionService._prepare_input(tmp_dir, req)
    assert prep_input[0] == "myboolean"
    assert prep_input[1] is True

    inp = IOValue(name="myfile_ref", value="/tmp/test/file")
    req = AnyProtobuf()
    req.Pack(inp)
    prep_input = ActionExecutionService._prepare_input(tmp_dir, req)
    assert prep_input[0] == "myfile_ref"
    assert prep_input[1] == "/tmp/test/file"

    inp = IOValue(name="mydir_ref", value="/tmp/test")
    req = AnyProtobuf()
    req.Pack(inp)
    prep_input = ActionExecutionService._prepare_input(tmp_dir, req)
    assert prep_input[0] == "mydir_ref"
    assert prep_input[1] == "/tmp/test"

    # Send a file containing b"HelloWorld!" in 2 pieces.
    inp = IOFileValue(name="myfile", value=b"Hello", file_name="a_test_file.bin")
    req = AnyProtobuf()
    req.Pack(inp)
    prep_input = ActionExecutionService._prepare_input(tmp_dir, req)
    assert prep_input[0] == "myfile"
    assert prep_input[1].endswith("a_test_file.bin")
    inp = IOFileValue(name="myfile", value=b"World!", file_name="a_test_file.bin")
    req = AnyProtobuf()
    req.Pack(inp)
    prep_input = ActionExecutionService._prepare_input(tmp_dir, req)
    assert prep_input[0] == "myfile"
    assert prep_input[1].endswith("a_test_file.bin")
    with open(prep_input[1], mode="rb") as f:
        content = f.read()
        assert content == b"HelloWorld!"

    inp = IODirectoryValue(name="mydir", value=SMALL_ZIP_FILE, is_last_chunk=True)
    req = AnyProtobuf()
    req.Pack(inp)
    prep_input = ActionExecutionService._prepare_input(tmp_dir, req)
    assert prep_input[0] == "mydir"
    assert len(glob.glob(f"{prep_input[1]}/**", recursive=True)) == 7


def _proto_Any_to_IO(msg: AnyProtobuf):
    output_file_type_str = msg.output.TypeName().split(".")[-1]
    output_type = getattr(execution_v3_pb2, output_file_type_str)
    output = output_type()
    msg.output.Unpack(output)
    return output_file_type_str, output


def _prepare_output_to_list(action_io_type: ActionIOType, output_value: Any):
    return [
        output
        for output in ActionExecutionService._prepare_output(
            action_io_type, output_value
        )
    ]


@pytest.mark.asyncio
async def test_prepare_outputs_STRING() -> None:
    outputs = _prepare_output_to_list(
        ActionIOType(type=IOType.Types.Name(2), name="my_output", optional=False),
        "myvalue",
    )
    assert len(outputs) == 1
    iotype, output = _proto_Any_to_IO(outputs[0])
    assert iotype == "IOValue"
    assert output.name == "my_output"
    assert output.value == "myvalue"


@pytest.mark.skip("We removed bytes value")
@pytest.mark.asyncio
async def test_prepare_outputs_BYTES() -> None:
    outputs = _prepare_output_to_list(
        ActionIOType(type=IOType.Types.Name(1), name="my_output", optional=False),
        b"myvalue",
    )
    assert len(outputs) == 1
    iotype, output = _proto_Any_to_IO(outputs[0])
    assert iotype == "IOBytesValue"
    assert output.name == "my_output"
    assert output.value == b"myvalue"


@pytest.mark.asyncio
async def test_prepare_outputs_FLOAT() -> None:
    outputs = _prepare_output_to_list(
        ActionIOType(type=IOType.Types.Name(6), name="my_output", optional=False),
        123.456,
    )
    assert len(outputs) == 1
    iotype, output = _proto_Any_to_IO(outputs[0])
    assert iotype == "IOFloatValue"
    assert output.name == "my_output"
    assert abs(output.value - 123.456) < 0.0001


@pytest.mark.asyncio
async def test_prepare_outputs_BOOLEAN() -> None:
    outputs = _prepare_output_to_list(
        ActionIOType(type=IOType.Types.Name(7), name="my_output", optional=False), False
    )
    assert len(outputs) == 1
    iotype, output = _proto_Any_to_IO(outputs[0])
    assert iotype == "IOBooleanValue"
    assert output.name == "my_output"
    assert output.value is False


@pytest.mark.asyncio
async def test_prepare_outputs_FILE() -> None:
    try:
        outputs = _prepare_output_to_list(
            ActionIOType(type=IOType.Types.Name(9), name="my_output", optional=False),
            "/this/file/does/not/exist",
        )
        assert False
    except Exception:
        pass

    # Empty file
    with tempfile.NamedTemporaryFile() as fp:
        outputs = _prepare_output_to_list(
            ActionIOType(type=IOType.Types.Name(9), name="my_output", optional=False),
            fp.name,
        )
        assert len(outputs) == 1
        iotype, output = _proto_Any_to_IO(outputs[0])
        assert iotype == "IOFileValue"
        assert output.name == "my_output"
        assert output.value == b""
        assert output.file_name == os.path.basename(fp.name)

    # A file bigger than LEN_DATA_SENT
    with tempfile.NamedTemporaryFile() as fp:
        for i in range(0, int(LEN_DATA_SENT * 1.1)):
            fp.write(b"x")
        fp.flush()
        outputs = _prepare_output_to_list(
            ActionIOType(type=IOType.Types.Name(9), name="my_output", optional=False),
            fp.name,
        )
        assert len(outputs) == 2
        iotype1, output1 = _proto_Any_to_IO(outputs[0])
        assert iotype1 == "IOFileValue"
        assert output1.name == "my_output"
        assert len(output1.value) == LEN_DATA_SENT
        assert output1.file_name == os.path.basename(fp.name)
        iotype2, output2 = _proto_Any_to_IO(outputs[1])
        assert iotype2 == "IOFileValue"
        assert output2.name == "my_output"
        assert output2.file_name == os.path.basename(fp.name)
        assert len(output1.value) + len(output2.value) == int(LEN_DATA_SENT * 1.1)


@pytest.mark.asyncio
async def test_prepare_outputs_DIRECTORY() -> None:
    try:
        outputs = _prepare_output_to_list(
            ActionIOType(type=IOType.Types.Name(11), name="my_output", optional=False),
            "/this/dir/does/not/exist",
        )
        assert False
    except Exception:
        pass

    # Completely empty dir
    with tempfile.TemporaryDirectory() as dirname:
        outputs = _prepare_output_to_list(
            ActionIOType(type=IOType.Types.Name(11), name="my_output", optional=False),
            dirname,
        )
        assert len(outputs) == 1
        iotype, output = _proto_Any_to_IO(outputs[0])
        assert iotype == "IODirectoryValue"
        assert output.name == "my_output"
        assert output.is_last_chunk is True
        assert output.value[:2] == b"PK"  # Test zip magick number
        assert len(output.value) < 200  # 200 bytes for an empty zip is more than enough

    with tempfile.TemporaryDirectory() as dirname:
        # A file bigger than LEN_DATA_SENT
        fp = open(f"{dirname}/a_big_file.bin", "wb")
        random.seed("I Love Ryax")
        fp.write(random.randbytes(int(LEN_DATA_SENT * 1.4)))
        fp.close()
        # an empty dir
        os.makedirs(f"{dirname}/an_empty_dir")
        # a dir containing 2 file
        os.makedirs(f"{dirname}/2file1dir")
        fp = open(f"{dirname}/2file1dir/file1", "wb")
        fp.write(b"FILE1")
        fp.close()
        fp = open(f"{dirname}/2file1dir/file2.bin.bin.bin", "wb")
        fp.write(b"FILE2")
        fp.close()

        outputs = _prepare_output_to_list(
            ActionIOType(type=IOType.Types.Name(11), name="my_output", optional=False),
            dirname,
        )

        assert len(outputs) == 2

        iotype, output1 = _proto_Any_to_IO(outputs[0])
        assert iotype == "IODirectoryValue"
        assert output1.name == "my_output"
        assert output1.value[:2] == b"PK"  # Test zip magick number
        assert output1.is_last_chunk is False

        iotype, output2 = _proto_Any_to_IO(outputs[1])
        assert iotype == "IODirectoryValue"
        assert output2.name == "my_output"
        assert output2.is_last_chunk is True

        # Unpack and check content of the zip file
        with tempfile.NamedTemporaryFile(suffix=".zip") as zipfile:
            zipfile.write(output1.value)
            zipfile.write(output2.value)
            outdir = f"{dirname}/OUT"
            os.makedirs(outdir)
            shutil.unpack_archive(zipfile.name, extract_dir=outdir)
            with open(f"{outdir}/2file1dir/file1", "rb") as fp:
                assert fp.read() == b"FILE1"
