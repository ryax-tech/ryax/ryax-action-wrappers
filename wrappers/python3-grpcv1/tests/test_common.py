# Copyright 2022 Ryax Technologies
# Use of this trigger code is governed by a BSD-style
# license that can be found in the LICENSE file.
import os
import sys
import tempfile
from pathlib import Path

import grpc
import pytest
from google.protobuf.empty_pb2 import Empty
from ryax_execution.service import ActionExecutionService


class ServicerContextMockup(grpc.ServicerContext):
    def invocation_metadata(self) -> None:
        raise NotImplementedError()

    def peer(self) -> None:
        raise NotImplementedError()

    def peer_identities(self) -> None:
        raise NotImplementedError()

    def peer_identity_key(self) -> None:
        raise NotImplementedError()

    def auth_context(self) -> None:
        raise NotImplementedError()

    def set_compression(self, compression) -> None:
        raise NotImplementedError()

    def send_initial_metadata(self, initial_metadata) -> None:
        raise NotImplementedError()

    def set_trailing_metadata(self, trailing_metadata) -> None:
        raise NotImplementedError()

    def trailing_metadata(self) -> None:
        raise NotImplementedError()

    def abort(self, code, details) -> None:
        raise NotImplementedError()

    def abort_with_status(self, status) -> None:
        raise NotImplementedError()

    def set_code(self, code) -> None:
        raise NotImplementedError()

    def set_details(self, details) -> None:
        raise NotImplementedError()

    def code(self) -> None:
        raise NotImplementedError()

    def details(self) -> None:
        raise NotImplementedError()

    def disable_next_message_compression(self) -> None:
        raise NotImplementedError()

    def is_active(self) -> None:
        raise NotImplementedError()

    def time_remaining(self) -> None:
        raise NotImplementedError()

    def cancel(self) -> None:
        raise NotImplementedError()

    def add_callback(self, callback) -> None:
        raise NotImplementedError()


@pytest.mark.asyncio
async def test_version() -> None:
    service = ActionExecutionService(clean_files_between_execs=False)
    req = Empty()
    ctx = ServicerContextMockup()
    rep = service.Version(req, ctx)
    assert "." in rep.version


@pytest.mark.asyncio
async def test_clean_files_but_not_cache():
    service = ActionExecutionService()
    test_content = b"Hello World!"
    with tempfile.TemporaryDirectory() as tmp_dir:
        tmp_dir = Path(tmp_dir)
        with open(tmp_dir / "to_remove", "wb") as test_file:
            test_file.write(test_content)
        tmp_sub_dir = tmp_dir / "sub_dir"
        tmp_sub_dir.mkdir()
        with open(tmp_sub_dir / "to_remove", "wb") as test_file:
            test_file.write(test_content)

        with tempfile.TemporaryDirectory() as home_dir:
            home_dir = Path(home_dir)
            cache_dir = Path(home_dir) / ".cache"
            cache_dir.mkdir()
            with open(cache_dir / "to_keep", "wb") as test_file:
                test_file.write(test_content)
            cache_sub_dir = cache_dir / "sub_dir"
            cache_sub_dir.mkdir()
            with open(cache_sub_dir / "to_keep", "wb") as test_file:
                test_file.write(test_content)

            with open(home_dir / "to_remove", "wb") as test_file:
                test_file.write(test_content)
            home_sub_dir = home_dir / "sub_dir"
            home_sub_dir.mkdir()
            with open(home_sub_dir / "to_remove", "wb") as test_file:
                test_file.write(test_content)

            assert cache_dir.exists()
            assert (cache_dir / "to_keep").exists()
            assert cache_sub_dir.exists()
            assert (cache_sub_dir / "to_keep").exists()
            assert (home_dir / "to_remove").exists()
            assert (home_sub_dir / "to_remove").exists()
            assert home_sub_dir.exists()
            assert (tmp_dir / "to_remove").exists()
            assert (tmp_sub_dir / "to_remove").exists()
            assert tmp_dir.exists()

            service._cleanup_execution(tmp_dir=tmp_dir, user_home=home_dir)

            assert cache_dir.exists()
            assert (cache_dir / "to_keep").exists()
            assert cache_sub_dir.exists()
            assert (cache_sub_dir / "to_keep").exists()

            assert not (home_dir / "to_remove").exists()
            assert not (home_sub_dir / "to_remove").exists()
            assert not home_sub_dir.exists()
            assert home_dir.exists()

            assert not (tmp_dir / "to_remove").exists()
            assert not (tmp_sub_dir / "to_remove").exists()
            assert tmp_dir.exists()


@pytest.mark.asyncio
async def test_print_logs_with_long_lines(capsys):
    service = ActionExecutionService()
    os.environ[
        "RYAX_ACTION_ENTRYPOINT"
    ] = f"{sys.executable} -c 'import random; print(\"toto\"*(2000 * 1024))'"
    with open(Path(service.tmp_io_dir.name) / "outputs.json", mode="w") as inputs_json:
        inputs_json.write("{}")
    await service._run_user_code({})
    captured = capsys.readouterr()
    assert len(captured.out) == (2000 * 1024 * 4 + 1)
    assert captured.out == "toto" * (2000 * 1024) + "\n"
