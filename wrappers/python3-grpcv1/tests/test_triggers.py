# Copyright 2022 Ryax Technologies
# Use of this trigger code is governed by a BSD-style
# license that can be found in the LICENSE file.
from typing import AsyncIterator, Callable

import pytest
from google.protobuf.any_pb2 import Any as AnyProtobuf
from ryax_execution import execution_v3_pb2
from ryax_execution.execution_v3_pb2 import (
    InitReply,
    InitRequest,
    IOType,
    IOValue,
    RunReply,
    RunRequest,
)
from ryax_execution.service import TriggerActionExecutionService
from test_common import ServicerContextMockup


@pytest.mark.asyncio
async def test_trigger_no_inputs_no_outputs() -> None:
    service = TriggerActionExecutionService(clean_files_between_execs=False)

    req = InitRequest(output_types=[])
    ctx = ServicerContextMockup()
    init_rep: InitReply = service.Init(req, ctx)
    assert init_rep.status == InitReply.Status.DONE
    assert init_rep.executor_id != ""
    assert init_rep.log_delimiter != ""

    def _import_run_user_code() -> (
        Callable[[TriggerActionExecutionService, dict], None]
    ):
        async def run(serv: TriggerActionExecutionService, input_values: dict) -> None:
            await service.create_run({})

        return run

    service._import_run_user_code = _import_run_user_code

    async def generator() -> AsyncIterator[RunRequest]:
        yield RunRequest()

    reps = []
    async for a in service.Run(generator(), ctx):
        reps.append(a)

    assert len(reps) == 1
    assert reps[0].last_reply_of_this_run is True
    assert reps[0].next_log_delimiter != init_rep.log_delimiter
    assert reps[0].status == RunReply.Status.DONE


@pytest.mark.asyncio
async def test_trigger_do_nothing() -> None:
    service = TriggerActionExecutionService(clean_files_between_execs=False)

    req = InitRequest(output_types=[])
    ctx = ServicerContextMockup()
    init_rep: InitReply = service.Init(req, ctx)
    assert init_rep.status == InitReply.Status.DONE
    assert init_rep.executor_id != ""
    assert init_rep.log_delimiter != ""

    def _import_run_user_code() -> (
        Callable[[TriggerActionExecutionService, dict], None]
    ):
        async def run(serv: TriggerActionExecutionService, input_values: dict) -> None:
            return

        return run

    service._import_run_user_code = _import_run_user_code

    async def generator() -> AsyncIterator[RunRequest]:
        yield RunRequest()

    reps = []
    async for a in service.Run(generator(), ctx):
        reps.append(a)

    assert len(reps) == 0


@pytest.mark.skip(
    "The check for an output that does not exist in the metatada has been removed and thus this test is obsolete."
)
@pytest.mark.asyncio
async def test_trigger_unknown_output() -> None:
    service = TriggerActionExecutionService(clean_files_between_execs=False)
    req = InitRequest(output_types=[])
    ctx = ServicerContextMockup()
    init_rep: InitReply = service.Init(req, ctx)
    assert init_rep.status == InitReply.Status.DONE
    assert init_rep.executor_id != ""
    assert init_rep.log_delimiter != ""

    def _import_run_user_code() -> (
        Callable[[TriggerActionExecutionService, dict], None]
    ):
        async def run(serv: TriggerActionExecutionService, input_values: dict) -> None:
            await serv.create_run({"unknown_outout": "ouch"})

        return run

    service._import_run_user_code = _import_run_user_code

    async def generator() -> AsyncIterator[RunRequest]:
        yield RunRequest()

    reps = []
    async for a in service.Run(generator(), ctx):
        reps.append(a)

    assert len(reps) == 1
    assert reps[0].last_reply_of_this_run is True
    assert reps[0].next_log_delimiter != init_rep.log_delimiter
    assert reps[0].status == RunReply.Status.ERROR


@pytest.mark.asyncio
async def test_trigger() -> None:
    service = TriggerActionExecutionService(clean_files_between_execs=False)
    req = InitRequest(output_types=[IOType(name="mystring", type=IOType.STRING)])
    ctx = ServicerContextMockup()
    init_rep: InitReply = service.Init(req, ctx)
    assert init_rep.status == InitReply.Status.DONE
    assert init_rep.executor_id != ""
    assert init_rep.log_delimiter != ""

    def _import_run_user_code() -> (
        Callable[[TriggerActionExecutionService, dict], None]
    ):
        async def run(serv: TriggerActionExecutionService, input_values: dict) -> None:
            assert input_values["myinput"] == "test_value"
            await serv.create_run({"mystring": "test_value"})

        return run

    service._import_run_user_code = _import_run_user_code

    async def generator() -> AsyncIterator[RunRequest]:
        inp = AnyProtobuf()
        inp.Pack(IOValue(name="myinput", value="test_value"))
        yield RunRequest(input=inp)

    reps = []
    async for a in service.Run(generator(), ctx):
        reps.append(a)

    assert len(reps) == 2

    output_file_type_str = reps[0].output.TypeName().split(".")[-1]
    output_type = getattr(execution_v3_pb2, output_file_type_str)
    output = output_type()
    reps[0].output.Unpack(output)

    assert output.value == "test_value"
    assert output.name == "mystring"

    assert reps[1].last_reply_of_this_run is True
    assert reps[1].next_log_delimiter != init_rep.log_delimiter
    assert reps[1].status == RunReply.Status.DONE


@pytest.mark.asyncio
async def test_trigger_cant_import_code() -> None:
    service = TriggerActionExecutionService(clean_files_between_execs=False)

    def _import_run_user_code() -> (
        Callable[[TriggerActionExecutionService, dict], None]
    ):
        raise ImportError

    service._import_run_user_code = _import_run_user_code

    req = InitRequest(output_types=[IOType(name="mystring", type=IOType.STRING)])
    ctx = ServicerContextMockup()
    init_rep: InitReply = service.Init(req, ctx)
    assert init_rep.status == InitReply.Status.DONE
    assert init_rep.executor_id != ""
    assert init_rep.log_delimiter != ""

    async def generator() -> AsyncIterator[RunRequest]:
        inp = AnyProtobuf()
        inp.Pack(IOValue(name="mystring", value="test_value"))
        yield RunRequest(input=inp)

    reps = []
    async for a in service.Run(generator(), ctx):
        reps.append(a)

    assert len(reps) == 1
    assert reps[0].last_reply_of_this_run is True
    assert reps[0].next_log_delimiter != init_rep.log_delimiter
    assert reps[0].status == RunReply.Status.ERROR


@pytest.mark.asyncio
async def test_trigger_user_code_fails() -> None:
    service = TriggerActionExecutionService(clean_files_between_execs=False)
    req = InitRequest(output_types=[IOType(name="mystring", type=IOType.STRING)])
    ctx = ServicerContextMockup()
    init_rep: InitReply = service.Init(req, ctx)
    assert init_rep.status == InitReply.Status.DONE
    assert init_rep.executor_id != ""
    assert init_rep.log_delimiter != ""

    def _import_run_user_code() -> (
        Callable[[TriggerActionExecutionService, dict], None]
    ):
        async def run(serv: TriggerActionExecutionService, input_values: dict) -> None:
            raise Exception("arg")

        return run

    service._import_run_user_code = _import_run_user_code

    async def generator() -> AsyncIterator[RunRequest]:
        inp = AnyProtobuf()
        inp.Pack(IOValue(name="myinput", value="test_value"))
        yield RunRequest(input=inp)

    reps = []
    async for a in service.Run(generator(), ctx):
        reps.append(a)

    assert len(reps) == 1
    assert reps[0].last_reply_of_this_run is True
    assert reps[0].next_log_delimiter != init_rep.log_delimiter
    assert reps[0].status == RunReply.Status.ERROR


@pytest.mark.asyncio
async def test_trigger_missing_output() -> None:
    service = TriggerActionExecutionService(clean_files_between_execs=False)
    req = InitRequest(output_types=[IOType(name="mystring", type=IOType.STRING)])
    ctx = ServicerContextMockup()
    init_rep: InitReply = service.Init(req, ctx)
    assert init_rep.status == InitReply.Status.DONE
    assert init_rep.executor_id != ""
    assert init_rep.log_delimiter != ""

    def _import_run_user_code() -> (
        Callable[[TriggerActionExecutionService, dict], None]
    ):
        async def run(serv: TriggerActionExecutionService, input_values: dict) -> None:
            assert input_values["myinput"] == "test_value"
            await serv.create_run({})

        return run

    service._import_run_user_code = _import_run_user_code

    async def generator() -> AsyncIterator[RunRequest]:
        inp = AnyProtobuf()
        inp.Pack(IOValue(name="myinput", value="test_value"))
        yield RunRequest(input=inp)

    reps = []
    async for a in service.Run(generator(), ctx):
        reps.append(a)
    print(reps)
    assert len(reps) == 1
    assert reps[0].last_reply_of_this_run is True
    assert reps[0].next_log_delimiter != init_rep.log_delimiter
    assert reps[0].status == RunReply.Status.ERROR


@pytest.mark.asyncio
async def test_trigger_wrong_input_type() -> None:
    service = TriggerActionExecutionService(clean_files_between_execs=False)
    req = InitRequest(output_types=[IOType(name="mystring", type=IOType.STRING)])
    ctx = ServicerContextMockup()
    init_rep: InitReply = service.Init(req, ctx)
    assert init_rep.status == InitReply.Status.DONE
    assert init_rep.executor_id != ""
    assert init_rep.log_delimiter != ""

    def _import_run_user_code() -> (
        Callable[[TriggerActionExecutionService, dict], None]
    ):
        async def run(serv: TriggerActionExecutionService, input_values: dict) -> None:
            assert input_values["myinput"] == "test_value"
            await serv.create_run({})

        return run

    service._import_run_user_code = _import_run_user_code

    async def generator() -> AsyncIterator[RunRequest]:
        inp = AnyProtobuf()
        inp.Pack(IOValue(name="myinput", value=123))
        yield RunRequest(input=inp)

    reps = []
    async for a in service.Run(generator(), ctx):
        reps.append(a)

    assert len(reps) == 1
    assert reps[0].last_reply_of_this_run is True
    assert reps[0].next_log_delimiter != init_rep.log_delimiter
    assert reps[0].status == RunReply.Status.ERROR
    assert "Traceback" in reps[0].error_message
    assert (
        "TypeError: bad argument type for built-in operation" in reps[0].error_message
    )
