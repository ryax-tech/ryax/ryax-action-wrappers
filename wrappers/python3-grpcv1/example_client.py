#!/usr/bin/env python3
import asyncio
import logging
import os
from logging import getLogger
from typing import Iterator, List

import grpc
from google import protobuf
from google.protobuf.empty_pb2 import Empty
from ryax_execution import execution_v3_pb2
from ryax_execution.execution_v3_pb2 import (
    InitReply,
    InitRequest,
    IOBooleanValue,
    IODirectoryValue,
    IOFileValue,
    IOFloatValue,
    IOIntegerValue,
    IOType,
    IOValue,
    RunRequest,
)
from ryax_execution.execution_v3_pb2_grpc import ActionExecutionStub

logger = getLogger(__name__)

LEN_DATA_SENT = 4_000_000 - 10_000  # keep some space for metainfos

SMALL_ZIP_FILE = (
    b"PK\x03\x04\x14\x00\x00\x00\x00\x00\xc1`\x88S\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x05"
    b"\x00\x00\x00adir/PK\x03\x04\x14\x00\x00\x00\x00\x00\xa2`\x88S\x8b\x9e\xd9\xd3\x01\x00\x00\x00\x01"
    b"\x00\x00\x00\n\x00\x00\x00adir/afileAPK\x03\x04\x14\x00\x00\x00\x00\x00\xc1`\x88S\x00\x00\x00\x00"
    b"\x00\x00\x00\x00\x00\x00\x00\x00\r\x00\x00\x00adir/asubdir/PK\x03\x04\x14\x00\x00\x00\x00\x00\x9c"
    b"`\x88S1\xcf\xd0J\x01\x00\x00\x00\x01\x00\x00\x00\x18\x00\x00\x00adir/asubdir/anotherfileBPK\x03"
    b"\x04\x14\x00\x00\x00\x00\x00\xaa`\x88S\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x0e\x00\x00"
    b"\x00adir/emptyfilePK\x03\x04\x14\x00\x00\x00\x00\x00\xc1`\x88S\x00\x00\x00\x00\x00\x00\x00\x00\x00"
    b"\x00\x00\x00\x0e\x00\x00\x00adir/emptydir/PK\x01\x02?\x03\x14\x00\x00\x00\x00\x00\xc1`\x88S\x00"
    b"\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x05\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\xedA"
    b"\x00\x00\x00\x00adir/PK\x01\x02?\x03\x14\x00\x00\x00\x00\x00\xa2`\x88S\x8b\x9e\xd9\xd3\x01\x00\x00"
    b"\x00\x01\x00\x00\x00\n\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\xa4\x81#\x00\x00\x00adir"
    b"/afilePK\x01\x02?\x03\x14\x00\x00\x00\x00\x00\xc1`\x88S\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00"
    b"\x00\x00\r\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\xedAL\x00\x00\x00adir/asubdir/PK\x01\x02"
    b"?\x03\x14\x00\x00\x00\x00\x00\x9c`\x88S1\xcf\xd0J\x01\x00\x00\x00\x01\x00\x00\x00\x18\x00\x00\x00"
    b"\x00\x00\x00\x00\x00\x00\x00\x00\xa4\x81w\x00\x00\x00adir/asubdir/anotherfilePK\x01\x02?\x03\x14"
    b"\x00\x00\x00\x00\x00\xaa`\x88S\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x0e\x00\x00\x00\x00"
    b"\x00\x00\x00\x00\x00\x00\x00\xa4\x81\xae\x00\x00\x00adir/emptyfilePK\x01\x02?\x03\x14\x00\x00\x00"
    b"\x00\x00\xc1`\x88S\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x0e\x00\x00\x00\x00\x00\x00\x00"
    b"\x00\x00\x00\x00\xedA\xda\x00\x00\x00adir/emptydir/PK\x05\x06\x00\x00\x00\x00\x06\x00\x06\x00d\x01"
    b"\x00\x00\x06\x01\x00\x00\x00\x00 "
)


async def run() -> None:
    server_ip = "localhost:8081"
    logger.info(f"Starting client, connecting to {server_ip}")
    async with grpc.aio.insecure_channel(server_ip) as channel:
        stub = ActionExecutionStub(channel)

        version = await stub.Version(Empty())
        logger.info(f"Action Wrapper version: {version.version}")
        response = await stub.Init(
            InitRequest(
                output_types=[
                    IOType(name="string", type=IOType.STRING),
                    IOType(name="integer", type=IOType.INTEGER),
                    IOType(name="float", type=IOType.FLOAT),
                    IOType(name="boolean", type=IOType.BOOLEAN),
                    IOType(name="file", type=IOType.FILE),
                    IOType(name="directory", type=IOType.DIRECTORY),
                ]
            )
        )
        logger.info(f"Init response received:\n{response}")
        if response.status == InitReply.ERROR:
            raise Exception(f"Error while initializing the execution: {response}")
        inputs = [
            IOValue(name="string", value="test_value"),
            IOIntegerValue(name="integer", value=123),
            IOFloatValue(name="float", value=0.123),
            IOBooleanValue(name="boolean", value=True),
            IOValue(name="file_ref", value="/tmp/test/file"),
            IOValue(name="dir_ref", value="/tmp/test"),
        ]
        filepath = "/tmp/test/file"
        with open(filepath, "rb") as output_file:
            data = output_file.read(LEN_DATA_SENT)
            while len(data) > 0:
                inputs.append(
                    IOFileValue(
                        name="file", value=data, file_name=os.path.basename(filepath)
                    )
                )
                data = output_file.read(LEN_DATA_SENT)

        inputs.append(
            IODirectoryValue(name="directory", value=SMALL_ZIP_FILE, is_last_chunk=True)
        )

        def generate_inputs(
            _inputs: List[protobuf.message.Message],
        ) -> Iterator[RunRequest]:
            for inp in _inputs:
                req = RunRequest()
                req.input.Pack(inp)
                yield req

        logger.info("Sending a run request")
        async for response in stub.Run(generate_inputs(inputs)):
            logger.info(f"Run response received with status: {response.status}")

            if response.last_reply_of_this_run:
                logger.info(
                    f"METADATA: {response.next_log_delimiter} {response.start_time}"
                )
            else:
                output_file_type_str = response.output.TypeName().split(".")[-1]
                output_type = getattr(execution_v3_pb2, output_file_type_str)
                output = output_type()
                response.output.Unpack(output)
                if output_file_type_str == "IOFileValue":
                    filepath = f"/tmp/RCV_{output.file_name}"
                    # TODO: remove file if already exists
                    with open(filepath, mode="ab") as input_file:
                        input_file.write(output.value)
                    logger.info(f"Data writtend to {filepath}")
                if output_file_type_str == "IODirectoryValue":
                    filename = f"/tmp/RCV_{output.name}.zip"
                    with open(filename, mode="ab") as input_file:
                        input_file.write(output.value)
                    logger.info(f"Data writtend to {filename}")
                else:
                    logger.info(output)


if __name__ == "__main__":
    logging.basicConfig(
        level=logging.INFO,
        format="%(asctime)s.%(msecs)03d %(levelname)-7s %(name)-22s %(message)s",
        datefmt="%Y-%m-%d,%H:%M:%S",
    )
    asyncio.run(run())
