#!/usr/bin/env python3
# Copyright 2022 Ryax Technologies
# Use of this trigger code is governed by a BSD-style
# license that can be found in the LICENSE file.
import os

from ryax_execution.service import main

if __name__ == "__main__":
    is_trigger_action = "RYAX_IS_TRIGGER_ACTION" in os.environ
    log_level = os.environ.get("RYAX_LOG_LEVEL", "INFO")
    clean_files_between_execs = "RYAX_DO_NOT_CLEAN_FILES" not in os.environ

    main(
        is_trigger_action=is_trigger_action,
        str_log_level=log_level,
        clean_files_between_execs=clean_files_between_execs,
    )
