# Copyright 2022 Ryax Technologies
# Use of this trigger code is governed by a BSD-style
# license that can be found in the LICENSE file.
import asyncio
import json
import logging
import os
import platform
import shlex
import shutil
import subprocess
import tempfile
import time
import traceback
import uuid
from asyncio import Task
from dataclasses import dataclass
from logging import getLogger
from pathlib import Path
from typing import (
    Any,
    AsyncIterator,
    Callable,
    Coroutine,
    Iterator,
    Optional,
    Tuple,
    Type,
    Union,
    List,
)

import grpc
from google.protobuf.any_pb2 import Any as AnyProtobuf
from google.protobuf.empty_pb2 import Empty
from google.protobuf.timestamp_pb2 import Timestamp

from . import execution_v3_pb2
from .execution_v3_pb2 import (
    InitReply,
    InitRequest,
    IOBooleanValue,
    IOBytesValue,
    IODirectoryValue,
    IOFileValue,
    IOFloatValue,
    IOIntegerValue,
    IOType,
    IOValue,
    RunReply,
    RunRequest,
    VersionReply,
)
from .execution_v3_pb2_grpc import (
    ActionExecutionServicer,
    add_ActionExecutionServicer_to_server,
)
from .ryax_trigger_protocol import RyaxRunStatus, RyaxTriggerProtocol

logger = getLogger(__name__)
__version__ = "0.1.0"

python_version = platform.python_version()

_cleanup_coroutines = []

LEN_DATA_SENT = 4_000_000 - 10_000  # keep some space for meta info
USER_DEFINED_ENV_PREFIX = "RYAX_USER_DEFINED_"

# WARNING: This should be in sync with the Runner
END_LOG_DELIMITER_PREFIX = "RYAX_EXECUTION_END---"
START_LOG_DELIMITER_PREFIX = "RYAX_EXECUTION_START---"

GPU_OOM_ERROR_CODE = 26


class OutputDoesNotExist(Exception):
    pass


class UserCodeShouldReturnDict(Exception):
    pass


class NotEnoughOutputs(Exception):
    pass


class WrapperExecutionError(Exception):
    pass


@dataclass
class UserDefinedErrorCase(Exception):
    code: int
    message: str


class GPUOutOfMemoryError(Exception):
    pass


def generate_log_delimiter() -> str:
    return str(uuid.uuid4())


def print_start_logs_delimiters(log_delimiter: str) -> None:
    # Print the log log_delimiter for the log service to know that the execution is starting
    print(START_LOG_DELIMITER_PREFIX + log_delimiter, flush=True)


def print_end_logs_delimiters(log_delimiter: str) -> None:
    # Print the log log_delimiter for the log service to know that the execution is starting
    print(END_LOG_DELIMITER_PREFIX + log_delimiter, flush=True)


@dataclass
class ActionIOType:
    name: str
    type: str
    optional: bool


class ActionExecutionService(ActionExecutionServicer):
    def __init__(self, clean_files_between_execs: bool = True):
        self.tmp_io_dir = tempfile.TemporaryDirectory()
        self.output_types: List[ActionIOType] = []
        self.log_delimiter: Optional[str] = None
        self.clean_files_between_execs: bool = clean_files_between_execs

    def Init(self, request: InitRequest, _: grpc.ServicerContext) -> InitReply:
        try:
            self.output_types = [
                ActionIOType(
                    name=output_type.name,
                    type=IOType.Types.Name(output_type.type),
                    optional=output_type.optional,
                )
                for output_type in request.output_types
            ]
            self.log_delimiter = generate_log_delimiter()
            logger.debug(f"Output types registered successfully: {self.output_types}")
            return InitReply(
                status=InitReply.DONE,
                log_delimiter=self.log_delimiter,
                executor_id=platform.node(),
            )
        except Exception as err:
            logger.exception(f"Unexpected error while processing Init ({str(err)})")
            return InitReply(status=InitReply.ERROR)

    @staticmethod
    def _prepare_input(
        input_files_dir: tempfile.TemporaryDirectory, raw_input: AnyProtobuf
    ) -> Optional[Tuple[str, Any]]:
        if (
            len(raw_input.ListFields()) == 0
        ):  # this is true when the module has no inputs
            return None
        input_type = getattr(execution_v3_pb2, raw_input.TypeName().split(".")[-1])
        input = input_type()
        raw_input.Unpack(input)

        if input_type in [
            IOValue,
            IOBytesValue,
            IOFloatValue,
            IOIntegerValue,
            IOBooleanValue,
        ]:
            return input.name, input.value
        elif input_type == IOFileValue:
            if input.file_name is None:
                raise Exception(
                    f"File name attribute is missing for the input file named '{input.name}'"
                )
            filename = Path(input_files_dir.name) / input.name / input.file_name
            os.makedirs(Path(input_files_dir.name) / input.name, exist_ok=True)
            with open(filename, mode="ab") as input_file:
                input_file.write(input.value)
            return input.name, filename.as_posix()
        elif input_type == IODirectoryValue:
            root = Path(input_files_dir.name) / input.name
            file_path = root / "raw.zip"
            os.makedirs(root, exist_ok=True)
            with open(file_path, mode="ab") as input_file:
                input_file.write(input.value)
            if input.is_last_chunk:
                extract_to = root / input.name
                os.makedirs(extract_to, exist_ok=True)
                shutil.unpack_archive(file_path, extract_dir=extract_to)
                os.remove(file_path)
                return input.name, extract_to.as_posix()
        else:
            raise Exception(f"Unsupported IO type: '{input_type}'")
        return None

    @staticmethod
    def _prepare_output(
        output_type: ActionIOType, output_value: Any
    ) -> Iterator[RunReply]:
        found_output_type = type(output_value)
        output_type_integer_value = IOType.Types.Value(output_type.type)
        if output_type_integer_value in [
            IOType.STRING,
            IOType.LONGSTRING,
            IOType.PASSWORD,
            IOType.ENUM,
        ]:
            packed_output = AnyProtobuf()
            packed_output.Pack(IOValue(name=output_type.name, value=str(output_value)))
            yield RunReply(output=packed_output)
        elif output_type_integer_value == IOType.INTEGER:
            if found_output_type is not int:
                raise Exception(
                    f"Invalid type for output '{output_type.name}'. Wanted integer, found {found_output_type}"
                )
            packed_output = AnyProtobuf()
            packed_output.Pack(
                IOIntegerValue(name=output_type.name, value=output_value)
            )
            yield RunReply(output=packed_output)
        elif output_type_integer_value == IOType.FLOAT:
            # accept int as a subset of float, this accepts 2 and 2.0
            if found_output_type is not float and not found_output_type is not int:
                raise Exception(
                    f"Invalid type for output '{output_type.name}'. Wanted float, found {found_output_type}"
                )
            packed_output = AnyProtobuf()
            packed_output.Pack(IOFloatValue(name=output_type.name, value=output_value))
            yield RunReply(output=packed_output)
        elif output_type_integer_value == IOType.BOOLEAN:
            if found_output_type is not bool:
                raise Exception(
                    f"Invalid type for output '{output_type.name}'. Wanted boolean, found {found_output_type}"
                )
            packed_output = AnyProtobuf()
            packed_output.Pack(
                IOBooleanValue(name=output_type.name, value=output_value)
            )
            yield RunReply(output=packed_output)

        elif output_type_integer_value == IOType.FILE:
            if not Path(output_value).exists():
                raise Exception(
                    f"The output reference '{output_value}' for the output named '{output_type.name}' does not exists"
                )
            if not Path(output_value).is_file():
                raise Exception(
                    f"The output reference '{output_value}' for the output named '{output_type.name}' is not a file"
                )
            with open(output_value, "rb") as output_file:
                data = output_file.read(LEN_DATA_SENT)
                while True:
                    packed_output = AnyProtobuf()
                    packed_output.Pack(
                        IOFileValue(
                            name=output_type.name,
                            value=data,
                            file_name=os.path.basename(output_value),
                        )
                    )
                    yield RunReply(output=packed_output)
                    data = output_file.read(LEN_DATA_SENT)
                    if len(data) == 0:
                        break
        elif output_type_integer_value == IOType.DIRECTORY:
            out_dir = Path(output_value)
            if not out_dir.exists():
                raise Exception(
                    f"The directory '{output_value}' for the output named '{output_type.name}' does not exists"
                )
            if not out_dir.is_dir():
                raise Exception(
                    f"The directory '{output_value}' for the output named '{output_type.name}' is not a directory"
                )
            with tempfile.NamedTemporaryFile(suffix=".zip") as fp:
                shutil.make_archive(fp.name[: -len(".zip")], "zip", root_dir=out_dir)
                with open(fp.name, "rb") as output_file:
                    data = output_file.read(LEN_DATA_SENT)
                    while True:
                        packed_output = AnyProtobuf()
                        packed_output.Pack(
                            IODirectoryValue(
                                name=output_type.name,
                                value=data,
                                is_last_chunk=len(data) < LEN_DATA_SENT,
                            )
                        )
                        yield RunReply(output=packed_output)
                        data = output_file.read(LEN_DATA_SENT)
                        if len(data) == 0:
                            break
        else:
            raise Exception("Unsupported IO type.")

    def _cleanup_execution(self, tmp_dir="/tmp", user_home="/home/ryax") -> None:
        """In the container, /tmp and user home are the only writable places. Clean them between executions but keep the $HOME/.cache folder"""
        self.tmp_io_dir.cleanup()
        if self.clean_files_between_execs:
            try:
                subprocess.run(f"rm -rf {tmp_dir}/*", shell=True)
            except Exception:
                logger.exception(
                    f"Error while removing function temporary data in {tmp_dir}"
                )
            try:
                for root, dirs, files in os.walk(user_home, topdown=False):
                    if not root.startswith(f"{user_home}/.cache"):
                        for name in dirs:
                            directory_name = os.path.join(root, name)
                            if directory_name != f"{user_home}/.cache":
                                logger.debug(f"Removing directory {directory_name}")
                                try:
                                    os.removedirs(directory_name)
                                except FileNotFoundError:
                                    pass
                        for name in files:
                            filename = os.path.join(root, name)
                            logger.debug(f"Removing file {filename}")
                            os.remove(filename)

            except Exception:
                logger.exception(
                    f"Error while removing function temporary data in {user_home}"
                )

    def _init_executions(self) -> None:
        self.tmp_io_dir = tempfile.TemporaryDirectory()

    @staticmethod
    def _import_user_code() -> Callable[[dict], dict]:
        from ryax_handler import handle  # noqa

        return handle

    async def _run_user_code(self, input_dict) -> dict:
        wrapper_command = shlex.split(os.environ["RYAX_ACTION_ENTRYPOINT"])
        with open(Path(self.tmp_io_dir.name) / "inputs.json", mode="w") as inputs_json:
            json.dump(input_dict, inputs_json)
        logger.debug("Start wrapper subprocess")
        process = await asyncio.create_subprocess_exec(
            *wrapper_command,
            self.tmp_io_dir.name,
            stdout=asyncio.subprocess.PIPE,
            stderr=asyncio.subprocess.STDOUT,
            env={
                var_name.removeprefix(USER_DEFINED_ENV_PREFIX): var_value
                for var_name, var_value in os.environ.items()
                if var_name.startswith(USER_DEFINED_ENV_PREFIX)
            },
        )
        # Real time output of the action logs
        while True:
            if process.stdout is None:
                continue
            if process.stdout.at_eof():
                break
            stdout = (await process.stdout.read(n=(64 * 1024))).decode()
            if stdout:
                print(stdout, end="", flush=True)

        await process.wait()
        if process.returncode is not None and process.returncode > 0:
            if process.returncode == 69:
                # Return code 69 is raised by the wrapper when we have a RyaxException raised by the user.
                with open(
                    Path(self.tmp_io_dir.name) / "outputs.json"
                ) as errored_outputs:
                    errored_outputs_dict = json.load(errored_outputs)
                    raise UserDefinedErrorCase(
                        code=errored_outputs_dict["code"],
                        message=errored_outputs_dict["message"],
                    )
            if process.returncode == GPU_OOM_ERROR_CODE:
                raise GPUOutOfMemoryError()
            raise WrapperExecutionError(
                f"The wrapper process execution failed with exit code: {process.returncode}"
            )

        logger.debug("Wrapper subprocess ended")
        with open(Path(self.tmp_io_dir.name) / "outputs.json") as outputs_json:
            output_dict = json.load(outputs_json)
        await process.wait()
        return output_dict

    async def Run(
        self, request: AsyncIterator[RunRequest], context: grpc.ServicerContext
    ) -> AsyncIterator[RunReply]:
        exit_code = 0
        assert self.log_delimiter is not None
        print_start_logs_delimiters(self.log_delimiter)
        new_log_delimiter = generate_log_delimiter()

        start_time = Timestamp()
        start_time.GetCurrentTime()
        reply = None
        try:
            self._init_executions()

            input_dict = {}
            async for req in request:
                input_prepared = ActionExecutionService._prepare_input(
                    self.tmp_io_dir, req.input
                )
                if input_prepared is not None:
                    input_dict[input_prepared[0]] = input_prepared[1]

            # Do run user code
            output_dict = await self._run_user_code(input_dict)

            end_time = Timestamp()
            end_time.GetCurrentTime()

            if output_dict is not None:
                if not isinstance(output_dict, dict):
                    raise UserCodeShouldReturnDict(
                        f"The 'handle' function should return a dict, not {type(output_dict)}."
                    )
                not_sent_yet_outputs = set(
                    [output_type.name for output_type in self.output_types]
                )
                # send the output data
                for output_type in self.output_types:
                    if output_dict.get(output_type.name) is None:
                        continue
                    for rep in ActionExecutionService._prepare_output(
                        output_type=output_type,
                        output_value=output_dict.get(output_type.name),
                    ):
                        yield rep
                    not_sent_yet_outputs.remove(output_type.name)
                if not all(
                    output.optional
                    for output in self.output_types
                    if output.name in not_sent_yet_outputs
                ):
                    raise NotEnoughOutputs(
                        "The following outputs are defined in the metadata "
                        f"but are not returned by the code: {not_sent_yet_outputs}"
                    )
            # create the ending message, without data
            reply = RunReply(
                status=RunReply.DONE,
                next_log_delimiter=new_log_delimiter,
                end_time=end_time,
                start_time=start_time,
                last_reply_of_this_run=True,
            )
        except UserDefinedErrorCase as error:
            logger.debug("User Defined Wrapper Exit Case: %s", error)
            end_time = Timestamp()
            end_time.GetCurrentTime()
            reply = RunReply(
                status=RunReply.ERROR,
                next_log_delimiter=new_log_delimiter,
                start_time=start_time,
                end_time=end_time,
                last_reply_of_this_run=True,
                http_status_code=error.code,
                error_message=error.message,
            )
        except GPUOutOfMemoryError:
            logger.debug("GPU out of memory")
            end_time = Timestamp()
            end_time.GetCurrentTime()
            reply = RunReply(
                status=RunReply.ERROR,
                next_log_delimiter=new_log_delimiter,
                start_time=start_time,
                end_time=end_time,
                last_reply_of_this_run=True,
                error_message="GPU out of memory",
            )
            # If there's GPU Memory Kill, let the pod get error state with code 26, to let VPA know the OOM happens
            exit_code = GPU_OOM_ERROR_CODE
        except (Exception, GeneratorExit):
            logger.exception("Error: ")
            end_time = Timestamp()
            end_time.GetCurrentTime()
            reply = RunReply(
                status=RunReply.ERROR,
                next_log_delimiter=new_log_delimiter,
                start_time=start_time,
                end_time=end_time,
                last_reply_of_this_run=True,
            )
        finally:
            # Print the log log_delimiter for the log service to know that the execution is over
            assert self.log_delimiter is not None
            print_end_logs_delimiters(self.log_delimiter)
            yield reply
            self.log_delimiter = new_log_delimiter
            logger.debug("Execution is over, cleaning...")
            self._cleanup_execution()
            logger.debug("Cleaning done")
            if exit_code != 0:
                exit(exit_code)

    def Version(self, request: Empty, context: grpc.ServicerContext) -> VersionReply:
        return VersionReply(version=__version__)


class TriggerActionExecutionService(ActionExecutionService, RyaxTriggerProtocol):
    trigger_task: Optional[Task] = None

    def __init__(self, clean_files_between_execs: bool) -> None:
        # we want the client to not be able to create_run before the previous one has been sent
        self.message_queue: asyncio.Queue = asyncio.Queue(maxsize=1)
        super().__init__(clean_files_between_execs)
        # Sources are never cleaned
        self._init_executions()

    async def create_run(
        self,
        data: dict,
        running_time: float = 0.001,
        status: RyaxRunStatus = RyaxRunStatus.DONE,
    ) -> None:
        """
        Trigger a new execution.

        running_time is the time (in second) that this execution took.
                     the execution will start at NOW()-running_time
                     and end at NOW().
        """
        reply = RunReply()

        # Compute timestamps
        start_time_float = time.time() - running_time
        seconds = int(start_time_float)
        nanos = int((start_time_float - seconds) * 10**9)
        start_time = Timestamp(seconds=seconds, nanos=nanos)
        end_time = Timestamp()
        end_time.GetCurrentTime()
        try:
            if status == RyaxRunStatus.DONE:
                not_sent_yet_outputs = set(
                    [output_type.name for output_type in self.output_types]
                )
                # send output data
                for output_type in self.output_types:
                    if data.get(output_type.name) is None:
                        continue
                    for rep in ActionExecutionService._prepare_output(
                        output_type, data.get(output_type.name)
                    ):
                        await self.message_queue.put(rep)
                    not_sent_yet_outputs.remove(output_type.name)

                if not all(
                    output.optional
                    for output in self.output_types
                    if output.name in not_sent_yet_outputs
                ):
                    raise NotEnoughOutputs(
                        "The following outputs are defined in the metadata "
                        f"but are not returned by the code: {not_sent_yet_outputs}"
                    )

            # create the ending message, without data
            reply = RunReply(
                status=RunReply.Status.Value(status.name),
                end_time=end_time,
                start_time=start_time,
                last_reply_of_this_run=True,
            )
        except Exception as err:
            logger.exception("Error: ")
            reply = RunReply(
                status=RunReply.ERROR,
                start_time=start_time,
                end_time=end_time,
                last_reply_of_this_run=True,
                error_message=str(err) + "\n" + traceback.format_exc(),
            )
        finally:
            logger.debug("New execution created")
            await self.message_queue.put(reply)

    def get_output_definitions(self) -> List[ActionIOType]:
        return self.output_types

    @staticmethod
    def _import_run_user_code() -> (
        Callable[["TriggerActionExecutionService", dict], Coroutine[dict, None, None]]
    ):
        # This is the user code import form sources. It is not present right now
        from ryax_run import (  # noqa
            run,
        )

        return run

    async def Run(
        self, request: AsyncIterator[RunRequest], context: grpc.ServicerContext
    ) -> AsyncIterator[RunReply]:
        """
        Run the trigger service that gather external event to generate new executions.
        """

        try:
            input_values = {}
            async for req in request:
                input_prepared = ActionExecutionService._prepare_input(
                    self.tmp_io_dir, req.input
                )
                if input_prepared is not None:
                    input_values[input_prepared[0]] = input_prepared[1]

            run = self._import_run_user_code()

            if self.trigger_task is None:
                self.trigger_task = asyncio.create_task(
                    run(self, input_values), name="trigger_coroutine"
                )

            while True:
                queue_coro = asyncio.create_task(
                    self.message_queue.get(), name="q_coro"
                )
                await asyncio.wait(
                    {self.trigger_task, queue_coro}, return_when=asyncio.FIRST_COMPLETED
                )

                if queue_coro.done():
                    yield queue_coro.result()
                if self.trigger_task.done():
                    # before quitting the coroutines might fill the queue with some messages
                    while not self.message_queue.empty():
                        yield self.message_queue.get_nowait()
                    queue_coro.cancel()
                    if self.trigger_task.exception() is not None:
                        raise Exception from self.trigger_task.exception()
                    return
        except Exception as err:
            logger.exception("Error: ")
            end_time = Timestamp()
            end_time.GetCurrentTime()
            yield RunReply(
                status=RunReply.ERROR,
                start_time=end_time,
                end_time=end_time,
                last_reply_of_this_run=True,
                error_message=str(err) + "\n" + traceback.format_exc(),
            )
        finally:
            logger.debug("Execution is over, cleaning...")
            self._cleanup_execution()
            logger.debug("Cleaning done")


async def serve(
    ServiceClass: Type[ActionExecutionService],
    server_port: Union[str, int],
    clean_files_between_execs: bool,
) -> None:
    server = grpc.aio.server()
    add_ActionExecutionServicer_to_server(
        ServiceClass(clean_files_between_execs=clean_files_between_execs), server
    )
    server_ip = f"0.0.0.0:{server_port}"
    server.add_insecure_port(server_ip)
    logger.debug(f"Server is running on {server_ip}")
    await server.start()

    async def server_graceful_shutdown():
        logging.debug("Starting graceful shutdown...")
        # Shuts down the server with 5 seconds of grace period. During the
        # grace period, the server won't accept new connections and allow
        # existing RPCs to continue within the grace period.
        await server.stop(5)

    _cleanup_coroutines.append(server_graceful_shutdown())
    await server.wait_for_termination()

    logger.debug("Server stopped")


def main(
    is_trigger_action: bool,
    str_log_level: str,
    clean_files_between_execs: bool,
) -> None:
    # Setup logging
    numeric_level = getattr(logging, str_log_level.upper(), None)
    if not isinstance(numeric_level, int):
        raise ValueError("Invalid log level: %s" % str_log_level)
    logging.basicConfig(
        level=numeric_level,
        format="%(asctime)s.%(msecs)03d %(levelname)-7s %(name)-22s %(message)s",
        datefmt="%Y-%m-%d,%H:%M:%S",
    )
    logger.debug("Logging level is set to %s" % str_log_level.upper())
    logger.debug(f"Ryax wrapper GRPC version {__version__}")
    logger.debug(f"Python version {python_version}")

    logger.debug(
        f"Remove temporary data between executions: {clean_files_between_execs}"
    )
    if clean_files_between_execs:
        logger.debug("WARNING: /tmp will be cleaned between each execution.")

    # Configuration
    default_server_port: int = 8081
    server_port = os.environ.get("RYAX_MODULE_SERVER_PORT", default_server_port)

    if is_trigger_action:
        service_class = TriggerActionExecutionService
    else:
        service_class = ActionExecutionService

    loop = asyncio.get_event_loop()
    try:
        loop.run_until_complete(
            serve(service_class, server_port, clean_files_between_execs)
        )
    finally:
        loop.run_until_complete(*_cleanup_coroutines)
        loop.close()
