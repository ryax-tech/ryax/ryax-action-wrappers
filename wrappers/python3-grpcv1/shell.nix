{ pkgs ? import <nixpkgs> { } }:
let
  python = pkgs.python3;
  pythonEnv = python.withPackages (ps: with ps;[
    grpcio
    grpcio-tools
    mypy-protobuf
    setuptools
    pyyaml
  ]);
in
pkgs.mkShell {
  buildInputs = [ pythonEnv pkgs.python3Packages.mypy-protobuf ];
  shellHook = ''
    export LD_LIBRARY_PATH=${pkgs.stdenv.cc.cc.lib}/lib
  '';
}
