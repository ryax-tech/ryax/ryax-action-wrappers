#!/usr/bin/env bash

# For Debug
set -x
set -e

echo LD_LIBRARY_PATH=$LD_LIBRARY_PATH
echo EXTRA_SKOPEO_ARGS=$EXTRA_SKOPEO_ARGS
echo REGISTRY=$REGISTRY
if [ ! "$REGISTRY" = "" ]; then
  TEST_REGISTRY=$REGISTRY/
else
  TEST_REGISTRY=""
fi

clean_up() {
  if [ ! "$CLEANUP_ENV" = "false" ]; then
    set +e
    # Ignore in case of errors since it might have already cleaned in between tests
    docker logs test-docker-ctn || true
    echo "Cleaning testing environment..."
    docker rm -f test-docker-ctn || true
  fi
}
trap clean_up EXIT

pytest -ra $@ ./wrappers/python3-grpcv1/

rm -fr /tmp/echo_processor
cp -fr examples/echo_processor /tmp/echo_processor
cp -v languages/python3/ryax_wrapper.py /tmp/echo_processor
pytest -ra $@ ./wrappers/python3-filev1/


echo "==============================================Test python3 / python3-grpcv1-trigger =============================================="
TEST_IMG=test-image-grpcv1-trigger
./ryax-build ./examples/trigger_with_dependencies $TEST_IMG latest python3 python3-grpcv1-trigger $REGISTRY
docker run -d -ti -p 8081:8081 --name test-docker-ctn $TEST_REGISTRY$TEST_IMG:latest
sleep 1
./wrappers/python3-grpcv1/cli.py --server=localhost:8081 ./examples/trigger_with_dependencies/ryax_metadata.yaml \
  --msg="It is working!"
docker rm -f test-docker-ctn
sleep 1

echo "============================================== Test python3-cuda / python3-grpcv1 ========================================"
TEST_IMG=test-image-grpcv1-cuda
./ryax-build ./examples/processor_cuda_with_dependencies $TEST_IMG latest python3-cuda python3-grpcv1 $REGISTRY
docker run -d -ti -p 8081:8081 --name test-docker-ctn $TEST_REGISTRY$TEST_IMG:latest
sleep 1
./wrappers/python3-grpcv1/cli.py --server=localhost:8081 ./examples/processor_cuda_with_dependencies/ryax_metadata.yaml \
  --msg="It is working!"
docker rm -f test-docker-ctn
sleep 1

echo "============================================== Test python3 / python3-grpcv1 ============================================="
TEST_IMG=test-image-grpcv1
./ryax-build ./examples/processor_with_dependencies $TEST_IMG latest python3 python3-grpcv1 $REGISTRY
docker run -d -ti -p 8081:8081 --name test-docker-ctn $TEST_REGISTRY$TEST_IMG:latest
sleep 1
./wrappers/python3-grpcv1/cli.py --server=localhost:8081 ./examples/processor_with_dependencies/ryax_metadata.yaml \
  --msg="It is working!"
docker rm -f test-docker-ctn
sleep 1

echo "============================================== Test nodejs / python3-grpcv1 =============================================="
TEST_IMG=test-image-nodejs-grpcv1
./ryax-build ./examples/nodejs_uppercase $TEST_IMG latest nodejs python3-grpcv1 $REGISTRY
docker run -d -ti -p 8081:8081 --name test-docker-ctn $TEST_REGISTRY$TEST_IMG:latest
sleep 1
./wrappers/python3-grpcv1/cli.py --server=localhost:8081 ./examples/nodejs_uppercase/ryax_metadata.yaml \
  --input_str="it is working"
docker rm -f test-docker-ctn
sleep 1

echo "============================================== Test csharp-dotnet6 / python3-grpcv1 ======================================"
TEST_IMG=test-image-csharp-dotnet6
./ryax-build ./examples/csharp_rectangle $TEST_IMG latest csharp-dotnet6 python3-grpcv1 $REGISTRY
docker run -d -ti -p 8081:8081 --name test-docker-ctn $TEST_REGISTRY$TEST_IMG:latest
sleep 1
./wrappers/python3-grpcv1/cli.py --server=localhost:8081 ./examples/csharp_rectangle/ryax_metadata.yaml \
  --length=2 \
  --width=4
docker rm -f test-docker-ctn
sleep 1

echo "============================================== Test python3 / python3-filev1=============================================="
TEST_IMG=test-image
./ryax-build ./examples/echo_processor $TEST_IMG latest python3 python3-filev1 $REGISTRY
TEMP_DIR=`mktemp -d`
echo \
'{
    "test_dir": "/iodir/inputs/test-dir.zip",
    "test_file": "/iodir/inputs/test-file.txt",
    "test_float": 2.5,
    "test_int": 25
}' > $TEMP_DIR/inputs.json
cat $TEMP_DIR/inputs.json
mkdir -p $TEMP_DIR/inputs
mkdir -p $TEMP_DIR/outputs
echo "hello boys and girls and all" > $TEMP_DIR/inputs/test-file.txt
cp examples/echo_processor/test-dir.zip $TEMP_DIR/inputs
echo "a.txt" > $TEMP_DIR/inputs/a.txt
echo "b.txt" > $TEMP_DIR/inputs/b.txt
chmod -R 777 $TEMP_DIR
# fix podman error when policy.json not defined
mkdir -p $HOME/.config/containers
echo \
'{
    "default": [
        {
            "type": "insecureAcceptAnything"
        }
    ]
}' > $HOME/.config/containers/policy.json
podman run -dt -v $TEMP_DIR:/iodir --name test-docker-ctn $TEST_REGISTRY${TEST_IMG}-python3-filev1:latest
sleep 5
podman logs test-docker-ctn
cat $TEMP_DIR/outputs.json | jq -r
rm -fr $TEMP_DIR || true
