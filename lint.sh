#!/usr/bin/env bash

set -e
# For debug
# set -x

while getopts "fd:" opt; do
  case "$opt" in
    f)
      DO_CHECK_ONLY="false"
      ;;
    d)
      CHECK_DIR="$OPTARG"
      ;;
    ?)
      echo "script usage: $(basename $0) [-f] [-d directory]" >&2
      exit 1
      ;;
  esac
done

TO_CHECK_DIR=${CHECK_DIR:-"."}
CHECK_ONLY=${DO_CHECK_ONLY:-"true"}

if [[ $CHECK_ONLY == "true" ]]
then
    BLACK_EXTRA_OPTS="--check --diff"
    RUFF_OPTS="--diff"
fi

echo "-- Checking python formating"
black $TO_CHECK_DIR --exclude "docs|ci|ryaxpkgs|migrations|.*pb2.*.py|.venv|.poetry" $BLACK_EXTRA_OPTS

echo "-- Checking import sorting"
ruff check $RUFF_EXTRA_OPTS $TO_CHECK_DIR

#echo "-- Format Nix files"
#nix fmt .


# Deactivated during first phases of dev.
# echo "-- Checking type annotations"
# mypy ./python3-grpcv1
