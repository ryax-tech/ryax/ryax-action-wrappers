{
  inputs.nixpkgs.url = "github:nixos/nixpkgs/nixos-24.11";
  inputs.nix2containerInput.url = "github:attilaersek/nix2container";
  inputs.nix2containerInput.inputs.nixpkgs.follows = "nixpkgs";
  inputs.flakeUtils.follows = "nix2containerInput/flake-utils";

  # Part to configure actions dependencies
  inputs.nixpkgs-python.url = "github:cachix/nixpkgs-python";
  # This one is the default
  inputs.actionNixpkgs.url = "github:nixos/nixpkgs/nixos-24.11";

  outputs = inputs@{ self, nixpkgs, nix2containerInput, flakeUtils, actionNixpkgs, nixpkgs-python }:
    flakeUtils.lib.eachDefaultSystem
      (system:
        let
          pkgs = import nixpkgs { inherit system; };
          nix2container = nix2containerInput.packages.${system}.nix2container;
          actionDir = "/tmp/ryax/actionDir";
          python = pkgs.python3;
          ryax-lib = import ./nix/lib.nix { inherit pkgs python; };
        in
        {

          packages = {
            ryaxBuild = import ./default.nix { inherit ryax-lib system nixpkgs-python actionNixpkgs actionDir nix2container; };
            test = ryax-lib.test;
            lint = ryax-lib.lint;
            default = pkgs.writeShellScriptBin "ryax-build" ''
              ./ryax-build $@
            '';
            pkgs = pkgs;
          };
          formatter = pkgs.nixpkgs-fmt;
          devShell = pkgs.mkShell {
            buildInputs = [ python pkgs.poetry pkgs.nix ];
            shellHook = ''
              export LD_LIBRARY_PATH=${pkgs.stdenv.cc.cc.lib}/lib
            '';
          };
        }
      );
}
