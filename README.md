# Ryax Action Wrappers

Define action wrappers with Nix functions to build a wrapped action ready to
accept requests.

## Usage

### Build and run an Action

```sh
./ryax-build ./examples/echo_processor test-grpcv1 1.0 python3 python3-grpcv1
```

And run with:
```sh
docker run --rm -ti --name test \
  -p 0.0.0.0:8081:8081 \
  -e RYAX_DO_NOT_CLEAN_FILES=1 -e RYAX_LOG_LEVEL=debug \
  -v /tmp:/tmp \
  test-grpcv1:1.0
```

Create some test files to test file reference mechanism:
```bash
rm -rf /tmp/test
mkdir /tmp/test
echo "TEST" > /tmp/test/file
```

Now, in another terminal, have to run the gRPC example client with:
```shell
cd wrappers/python3-grpcv1
poetry run \
  /cli.py --output-dir=$(mktemp -d) ../../examples/echo_processor/ryax_metadata.yaml \
  --test_str="a string" --password="pass" --test_int=6 --test_float=45 --longstring="longstr" --test_file=./cli.py --test_dir=/tmp/test --enum=value1
```

You will see a message with all types of input. 

Stop and remove the container with:
```shell
docker kill test
```

### Build an run a Trigger

```sh
./ryax-build ./examples/trigger test-grpcv1-trigger 1.0 python3 python3-grpcv1-trigger
```
And run with:
```sh
docker run --rm -ti --name test \
  -p 0.0.0.0:8081:8081 \
  -e RYAX_DO_NOT_CLEAN_FILES=1 -e RYAX_LOG_LEVEL=debug \
  -v /tmp:/tmp \
  test-grpcv1-trigger:1.0
```

Run the CLI:
```shell
cd wrappers/python3-grpcv1
poetry run \
  ./cli.py --output-dir=$(mktemp -d) ../../examples/trigger/ryax_metadata.yaml \
  --string="a string" --password="pass" --integer=6 --float=45 --longstring="longstr" --file=./cli.py --directory=/tmp/test --enum=value1
```
In the case of the trigger you'll see a test error message and a
normal message alternating every 5 seconds.

Stop and remove the container with:
```shell
docker kill test
```

## Development

Run the linter with:
```bash
./lint.sh
```

Note that we do not run mypy on the whole project because it is split for packaging reason bu you can run mypy on wrappers directly. For example:
```bash
cd wrappers/python3-grpcv1
mypy ./wrapper.py --explicit-package-bases
```
**WARNING**: When you develop the gRPC interface be careful to not break retro-compatibility.

First update the protobuf generated libraries:
```sh
cd wrappers/python3-grpcv1
python -m grpc_tools.protoc  --mypy_out=. -I./ --python_out=. --grpc_python_out=. --mypy_grpc_out=. ./ryax_execution/*.proto
```

## Example of usage on external machine using singularity

### Build image

You can build the docker image tar gz for test action, need to execute from the action_wrapper directory.
```sh
./ryax-build ./examples/echo_processor test-file-processor 1.0 python3 python3-filev1
```

### Convert image ot singularity

The simplest way is to generate the singularity locally, need to install singularity for that.
```sh
singularity build --fakeroot test-file-processor.sif docker-archive://result
```

### Run manually

First you will need to create the desired input in folder, create the output folder.

```sh
mkdir -p iodir/inputs
mkdir -p iodir/outputs
```

Create a `input.json` file. And for every directory or file create it on the `iodir`. 

```shell
cat <<EOF >> iodir/inputs.json
{
    "test_str": "name",
    "test_dir": "/iodir/inputs/test-dir",
    "test_file": "/iodir/inputs/test-file.txt",
    "test_float": 2.5,
    "test_int": 25
}
EOF
echo "hello boys and girls and all" > iodir/inputs/test-file.txt
mkdir -p iodir/inputs/test-dir
echo "i have a gun" > iodir/inputs/test-dir/file_A.txt
echo "jane has a gun too" > iodir/inputs/test-dir/file_B.txt  
```

Now you can finally run using singularity, need to bind the folder to `/iodir`.

```shell
singularity run --bind $PWD/iodir:/iodir test-file-processor.sif
```

Now check the output is available on the shared directory.

```shell
$ ls -la iodir/outputs
total 0
drwxr-xr-x 3 pvelho users 4096 Jan 12 14:30 .
drwxr-xr-x 4 pvelho users 4096 Jan 12 13:57 ..
-rw-r--r-- 1 pvelho users  108 Jan 12 14:30 output.json
drwxr-xr-x 2 pvelho users 4096 Jan 12 14:30 test_dir
-rw-r--r-- 1 pvelho users    5 Jan 12 14:30 test_file_out.txt
```

Run tfdetection, if you fill like it you can try with a more complex action like the one for 
video-detection. For that use the input parameters below. Create a folder images and grab the
images you want to apply object detection.

```json
{
    "model" : "ssdlite_mobilenet_v2_coco_2018_05_09",
    "images" : "/iodir/inputs/images"
}
```

## Add the support of a new language

To add another language support we use the `python3-grpcv1` wrapper.

Create a directory with the name of the new language/runtime that you want to support in the `wrappers` directory.
Add a wrapper in the targeted language that load input from a Json file, run the action and write the outputs to a Json file.
You also need to add a Nix file with the definition of how to install the dependencies, create the layers, and set entrypoint and environment variables.

See `./wrappers` to have examples.

Don't forget to import your language in the set in `./wrappers/default.nix`

After that you must modify ryax-repository and ryax-action-builder to add the support for the newly created type.

Examples:
- ryax-repository : https://gitlab.com/ryax-tech/ryax/ryax-repository/-/commit/49d3fad45e357e31dd833fc4b9258e87b3fb7c56
- ryax-action-builder : https://gitlab.com/ryax-tech/ryax/ryax-action-builder/-/blob/master/ryax/action_builder/domain/action_build/action_build_values.py#L9
