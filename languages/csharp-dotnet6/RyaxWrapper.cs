// TODO: Use builtin instead to avoid external deps
using Newtonsoft.Json;

namespace Ryax
{
    public class Wrapper
    {
        static void Main(string[] args) {

            string ioDirPath;
            if (args.Length > 0) {
                ioDirPath = args[0];
            }
            else {
                ioDirPath = ".";
            }
            string inputsFilePath = Path.Combine(ioDirPath, "inputs.json");
            string outputFilePath = Path.Combine(ioDirPath, "outputs.json");
            string json = File.ReadAllText(inputsFilePath);

            var inputs = JsonConvert.DeserializeObject<Dictionary<string, object>>(json);

            // Call user code
            var outputs = Ryax.Handler.handler(inputs);

            json = JsonConvert.SerializeObject(outputs);
            File.WriteAllText(outputFilePath, json);
        }
    }
}
