{ pkgs, actionDir, nix2container }:
{
  install = pkgs.writeShellScriptBin "csharp-install" ''
    cp ${./RyaxWrapper.cs} ${actionDir}
    cd ${actionDir}
    echo Add wrapper dependencies
    ${pkgs.dotnet-sdk_6}/bin/dotnet add package Newtonsoft.Json -v 13.0.2
    ${pkgs.dotnet-sdk_6}/bin/dotnet publish -c Release -o out -p:StartupObject=Ryax.Wrapper
  '';
  # TODO: only import action in /data for python and /out in /data for c#
  layers = { uid, gid, user, group }: [
    (nix2container.buildLayer
      {
        copyToRoot = [
          (pkgs.buildEnv {
            name = "actionDotnetRuntime";
            paths = [ pkgs.dotnet-runtime_6 ];
            pathsToLink = "/bin";
          })
        ];
      })
  ];
  entrypoint =
    let

      projectName = with builtins; elemAt
        (elemAt
          (filter (elem: elem != null) (
            map (match "(.*).csproj") (attrNames (readDir actionDir)))
          ) 0) 0;
    in
    "dotnet /data/out/${projectName}.dll";
  environment = [ "DOTNET_CLI_TELEMETRY_OPTOUT=1" ];
}
