import {  writeFile, readFileSync } from "fs";
import handle from './ryax_handler.js';

const ioDir = process.argv[2];

//Read dictionary from inputs.json.
const ryax_inputs = JSON.parse(readFileSync(ioDir+'/inputs.json'));

//Call ryax_handler.handle with the input dictionary.
const ryax_outputs = handle(ryax_inputs)

//Retrieve the output dictionary, write it to outputs.
writeFile(ioDir+'/outputs.json', JSON.stringify(ryax_outputs), 'utf8', (err) => { if (err != null) { console.log(err); } });
