{ pkgs, actionDir, nix2container }:
{
  install = pkgs.writeShellScriptBin "nodejs" ''
    cp ${./RyaxWrapper.js} ${actionDir}/RyaxWrapper.js
    cd ${actionDir}
    if [ -f ${actionDir}/package.json ] ; then
      echo "Found package.json file, adding npm dependencies..."
      echo "WARNING these dependencies won't work if they require binary libraries, to do that check nixos.org and"
      echo "        add these dependencies directly to ryax_metadata.yaml dependencies list."
      export PATH=${pkgs.nodejs}/bin:$PATH
      ${pkgs.nodePackages.npm}/bin/npm install --prefix ${actionDir}
    fi
  '';
  layers = { uid, gid, user, group }: [
    (nix2container.buildLayer
      {
        copyToRoot = [
          (pkgs.buildEnv {
            name = "actionNodejsRuntime";
            paths = [ pkgs.nodejs ];
            pathsToLink = "/bin";
          })
        ];
      })
  ];
  entrypoint = "node /data/RyaxWrapper.js";
}
