{ actionDir, pythonSolver, actionNixpkgs, overlays, nix2container, extraEnvVars ? [ ], requirementsTxt, lockTmpDir }:
let
  pkgs = import actionNixpkgs {
    inherit overlays;
    config = {
      allowUnfree = true;
      cudaSupport = true;
    };
  };
  lib = pkgs.callPackage ../../nix/lib.nix { };
  python = pythonSolver pkgs;
  metadataDeps = lib.metadataDependencies2nix actionDir python.pkgs pkgs;
in
{
  install = pkgs.writeShellScriptBin "python-install" ''
    # set -x
    set -eu
    export PIP_DISABLE_PIP_VERSION_CHECK=1
    export PYTHONDONTWRITEBYTECODE=1
    export PYTHONHASHSEED=0
    export PYTHONUNBUFFERED=1
    export CUDA_PATH=${pkgs.cudatoolkit}
    export EXTRA_LDFLAGS="-L/lib -L${pkgs.linuxPackages.nvidia_x11}/lib"
    export EXTRA_CCFLAGS="-I/usr/include"

    # First we save the requirements.txt for saving the orinal version
    if [[ -f ${actionDir}/requirements.txt ]]
    then
      cat ${actionDir}/requirements.txt  > ${actionDir}/${lockTmpDir}/requirements.orig.txt
    fi

    cp ${requirementsTxt} ${actionDir}/requirements.txt

    echo Fetching Python dependencies...
    ${python.pkgs.pip}/bin/pip install --target "${actionDir}/.env" -r ${actionDir}/requirements.txt

    echo Freezing dependencies for reproducible build
    ${python.pkgs.pip}/bin/pip freeze --path "${actionDir}/.env" > "${actionDir}/${lockTmpDir}/requirements.lock.txt"

    if [[ -d ${actionDir}/.env ]]
    then
      # Remove bytecode to have a reproducible layer
      find "${actionDir}/.env" -type f -name "*.pyc" -delete
      find "${actionDir}/.env" -type d -name "__pycache__" -delete
    fi

    echo Inject the wrapper
    cp --no-preserve=mode,ownership ${./ryax_wrapper.py} ${actionDir}/ryax_wrapper.py
  '';
  layers = { uid, gid, user, group }:
    let
      fixSheBangScript = pkgs.writeScriptBin "fixSheBang" (builtins.readFile ./fix-shebang.py);
      importPython3ActionDeps = pkgs.runCommand "deps" { } ''
        set -eu
        echo Add python packages into the container
        mkdir -p $out/data
        # Support for pip packages
        if [ -d ${(/. + actionDir)}/.env ]; then
          cp -r ${(/. + actionDir)}/.env $out/data/.env
          if [ -d $out/.env/bin ]
          then
            ${python}/bin/python ${fixSheBangScript}/bin/fixSheBang $out/data/.env/bin/* -o $out/data/.env/bin/
          fi
        fi
      '';
      fixCudaLinks = pkgs.runCommand "deps" { } ''
        mkdir -p $out/lib64
        ln -s ${pkgs.glibc.out}/lib64/ld-linux-x86-64.so.2 $out/lib64/ld-linux-x86-64.so.2
      '';
    in
    [
      (nix2container.buildLayer {
        copyToRoot = [
          (pkgs.buildEnv {
            name = "root";
            paths = with pkgs; [
              git
              gitRepo
              gnupg
              autoconf
              curl
              procps
              gnumake
              util-linux
              m4
              gperf
              unzip
              cudatoolkit
              linuxPackages.nvidia_x11
              libGLU
              libGL
              expat.dev
              xorg.libXi
              xorg.libXmu
              freeglut
              xorg.libXext
              xorg.libX11
              xorg.libXv
              xorg.libXrandr
              zlib
              wget
              ncurses5
              stdenv.cc
              binutils
              bashInteractive
            ];
            pathsToLink = [ "/bin" "/lib" ];
          })
          fixCudaLinks
        ];
      })
      (nix2container.buildLayer {
        copyToRoot = [ importPython3ActionDeps ];
        perms = [
          {
            path = importPython3ActionDeps;
            regex = ".*";
            inherit uid gid user group;
          }
          {
            path = importPython3ActionDeps;
            regex = "\\.env/bin/.*";
            mode = "0755";
          }
        ];
        reproducible = false;
      })
    ];
  entrypoint =
    let
      env = python.withPackages (ps: with ps; metadataDeps);
    in
    "${env}/bin/python /data/ryax_wrapper.py";
  environment = [
    "PYTHONUNBUFFERED=True"
    "PYTHONPATH=/data:/data/.env"
    "PATH=/usr/bin:/bin:/data/.env/bin"
    "NVIDIA_DRIVER_CAPABILITIES=compute,utility"
    "LD_LIBRARY_PATH=/usr/lib64:/usr/local/nvidia/lib64:${pkgs.stdenv.cc.cc.lib}/lib:${pkgs.zlib}/lib"
  ] ++ extraEnvVars;
}
