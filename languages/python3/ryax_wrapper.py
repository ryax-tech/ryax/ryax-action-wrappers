import json
import os
import sys
from pathlib import Path

import ryax_handler  # noqa This will be availabe at runtime

if __name__ == "__main__":
    # Read environment from file if exists
    envfile = Path("/data/default.env")
    if envfile.is_file():
        with open(envfile, "r") as slurmenv:
            for line in slurmenv:
                (key, val) = line.split("=")
                os.environ[key] = val.rstrip()

    io_dir_path = sys.argv[1] if len(sys.argv) > 1 else "."

    inputs_file_path = Path(io_dir_path) / "inputs.json"
    output_file_path = Path(io_dir_path) / "outputs.json"

    with open(inputs_file_path, "r") as f:
        inputs = json.load(f)

    # Call user code
    try:
        outputs = ryax_handler.handle(inputs)
    except Exception as err:
        if err.__class__.__name__ == "RyaxException":
            errored_outputs = {
                "code": getattr(err, "code", 500),
                "message": getattr(err, "message", "Internal Server Error"),
            }
            with open(output_file_path, "w") as f:
                json.dump(errored_outputs, f)
            sys.exit(69)
        else:
            raise

    with open(output_file_path, "w") as f:
        json.dump(outputs, f)
