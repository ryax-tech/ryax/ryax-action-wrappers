{ pkgs, actionDir, python, nix2container, metadataDeps, extraEnvVars ? [ ], requirementsTxt, lockTmpDir }:
{
  install = pkgs.writeShellScriptBin "python-install" ''
    set -eu
    export PIP_DISABLE_PIP_VERSION_CHECK=1
    export PYTHONDONTWRITEBYTECODE=1
    export PYTHONHASHSEED=0
    export PYTHONUNBUFFERED=1

    # First we save the requirements.txt for saving the orinal version
    if [[ -f ${actionDir}/requirements.txt ]]
    then
      cat ${actionDir}/requirements.txt  > ${actionDir}/${lockTmpDir}/requirements.orig.txt
    fi

    cp ${requirementsTxt} ${actionDir}/requirements.txt

    echo Fetching Python dependencies...
    ${python.pkgs.pip}/bin/pip install --target "${actionDir}/.env" -r ${actionDir}/requirements.txt

    echo Freezing dependencies for reproducible build
    ${python.pkgs.pip}/bin/pip freeze --path "${actionDir}/.env" > "${actionDir}/${lockTmpDir}/requirements.lock.txt"

    if [[ -d ${actionDir}/.env ]]
    then
      # Remove bytecode to have a reproducible layer
      find "${actionDir}/.env" -type f -name "*.pyc" -delete
      find "${actionDir}/.env" -type d -name "__pycache__" -delete
    fi

    echo Inject the wrapper
    cp --no-preserve=mode,ownership ${./ryax_wrapper.py} ${actionDir}/ryax_wrapper.py
  '';
  layers = { uid, gid, user, group }:
    let
      fixSheBangScript = pkgs.writeScriptBin "fixSheBang" (builtins.readFile ./fix-shebang.py);
      importPython3ActionDeps = pkgs.runCommand "deps" { } ''
          set -eu
          echo Add python packages into the container
          mkdir -p $out/data
          # Support for pip packages
          if [ -d ${(/. + actionDir)}/.env ]; then
            cp -r ${(/. + actionDir)}/.env $out/data/.env
            if [ -d $out/.env/bin ]
            then
        	    ${python}/bin/python ${fixSheBangScript}/bin/fixSheBang $out/data/.env/bin/* -o $out/data/.env/bin/
            fi
          fi
      '';
    in
    [
      (nix2container.buildLayer
        {
          copyToRoot = [ importPython3ActionDeps ];
          perms = [
            {
              path = importPython3ActionDeps;
              regex = ".*";
              inherit uid gid user group;
            }
            {
              path = importPython3ActionDeps;
              regex = "\\.env/bin/.*";
              mode = "0755";
            }
          ];
          reproducible = false;
        }
      )
    ];
  entrypoint =
    let
      env = python.withPackages (ps: metadataDeps);
    in
    "${env}/bin/python /data/ryax_wrapper.py";
  environment = [
    "PYTHONUNBUFFERED=True"
    "PYTHONPATH=/data:/data/.env"
    "PATH=/usr/bin:/bin:/data/.env/bin"
  ] ++ extraEnvVars;
}
