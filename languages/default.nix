{ actionDir, actionNixpkgs, overlays, pkgs, python, pythonSolver, nix2container, metadataDeps, extraEnvVars, requirementsTxt, lockTmpDir }:
{
  python3 = import ./python3 { inherit actionDir pkgs python nix2container metadataDeps extraEnvVars requirementsTxt lockTmpDir; };

  python3-cuda = import ./python3-cuda {
    inherit actionDir pythonSolver actionNixpkgs overlays nix2container extraEnvVars requirementsTxt lockTmpDir;
  };

  csharp-dotnet6 = import ./csharp-dotnet6 { inherit actionDir pkgs nix2container; };

  nodejs = import ./nodejs { inherit actionDir pkgs nix2container; };
}
