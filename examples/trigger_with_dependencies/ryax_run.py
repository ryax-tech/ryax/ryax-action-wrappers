# Copyright 2022 Ryax Technologies
# Use of this trigger code is governed by a BSD-style
# license that can be found in the LICENSE file.
import time
from logging import getLogger
import subprocess

import pandas
from ryax_execution.ryax_trigger_protocol import RyaxTriggerProtocol

logger = getLogger(__name__)


async def run(service: RyaxTriggerProtocol, input_values: dict) -> None:
    """Check that imports works, create an execution and quit"""
    print(pandas.show_versions())

    process = subprocess.run(["cowsay", f"{input_values['msg']}"], capture_output=True)
    print(process.stdout.decode())

    process = subprocess.run(["pdfx", "--version"], capture_output=True)
    print(process.stdout.decode())

    logger.info("New execution")
    await service.create_run({"date": time.time()})
