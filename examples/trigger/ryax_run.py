# Copyright 2022 Ryax Technologies
# Use of this trigger code is governed by a BSD-style
# license that can be found in the LICENSE file.
import asyncio
import time
from logging import getLogger

from ryax_execution.ryax_trigger_protocol import RyaxTriggerProtocol

logger = getLogger(__name__)


async def run(service: RyaxTriggerProtocol, input_values: dict) -> None:
    while True:
        logger.info("New execution")
        await service.create_run({"date": time.time(), **input_values})
        await asyncio.sleep(5)
        logger.info("New error")
        await service.create_run_error()
        await asyncio.sleep(5)
