﻿using System;
using System.Diagnostics;

namespace Ryax {
    class Rectangle {

        // member variables
        public double length;
        public double width;

        public double GetArea() {
            return length * width;
        }
        public void Display() {
            Console.WriteLine("Length: {0}", length);
            Console.WriteLine("Width: {0}", width);
            Console.WriteLine("Area: {0}", GetArea());
        }
    }
    class Handler {
        public static Dictionary<string, object> handler(Dictionary<string, object> inputs) {
            Console.WriteLine("Inputs:");
            foreach (var item in inputs)
            {
                Console.WriteLine("- {0}: {1}", item.Key, item.Value);
            }

            Rectangle r = new Rectangle();
            r.length = (double) inputs["length"];
            r.width = (double) inputs["width"];
            r.Display();

            var outputs = new Dictionary<string, object>() {
                {"area", r.GetArea()}
            };

            return outputs;
        }

        static void Main(string[] args) {
            var inputs = new Dictionary<string, object>();
            inputs["width"] = 10.0;
            inputs["length"] = 10.0;
            Dictionary<string, object> outputs = handler(inputs);
            Console.WriteLine("Outputs: {0}", outputs);
            Debug.Assert((double)(outputs["area"]) == 100);
        }
    }
}
