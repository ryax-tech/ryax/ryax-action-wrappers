# Copyright 2022 Ryax Technologies
# Use of this trigger code is governed by a BSD-style
# license that can be found in the LICENSE file.
def handle(inputs: dict) -> dict:
    """Simple test echo handler"""
    print(f"ECHO PROCESSOR Inputs/outputs: {inputs}")
    return inputs
