# Copyright 2022 Ryax Technologies
# Use of this trigger code is governed by a BSD-style
# license that can be found in the LICENSE file.
import subprocess

import pandas


def handle(inputs: dict) -> dict:
    print(pandas.show_versions())

    process = subprocess.run(["cowsay", f"{inputs['msg']}"], capture_output=True)
    print(process.stdout.decode())

    process = subprocess.run(["pdfx", "--version"], capture_output=True)
    print(process.stdout.decode())

    return {}
