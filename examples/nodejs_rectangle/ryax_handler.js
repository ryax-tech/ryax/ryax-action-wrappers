export default function handle(ryax_input) {
    const width = ryax_input["width"];
    const height = ryax_input["height"];
    var output = {"area": width * height};
    return output;
}