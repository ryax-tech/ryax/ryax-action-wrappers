import { upperCase } from "upper-case";

export default function handle(ryax_input) {
    const output_str = upperCase(ryax_input["input_str"]);
    return { "output_str" : output_str };
}
